# Data Pipelines

This is the repo for data wrangling: we put all the stuff for pulling, normalizing and pushing data around our infrastructure.

This repo is organized as follow:

- a folder per data task (e.g.: gnc, for moving GNC data)
- a `lib/` folder for stuff shared among task, or general stuff (e.g.: API accesses).

## Pipeline guidelines

A data task is organised in different steps:

1. pulling data from the vendor (sometimes the data can be pulled). The raw data MUST be saved as-is in our S3 bucket. Archive can be expanded. Encrypted file MUST be decrypted.
2. normalizing data to fit our standard. That implies consistency, cleanup, anonymization...
3. pushing normalized data to our storage (S3 and/or database)
4. data processing (model calculation, BI, analytics...)


Tasks MUST be idempotent: running them twice should give the same results. Typically, as tasks have side effect by essence, that means the state of
the persistence layer must NOT be changed by running a task twice (it should be the same after the first run than after the second run).
 
Although there is necessarily ad-hoc processing for every tasks, effort should be made to factor most of the features in 
reusable libraries / tune the processing with configuration files.

Tasks MUST includes their own README.

### Data Persistence

We store the data in 3 different ways:

- Raw data (straight from the client) MUST be stored in `s3://engaging-datalake/raw/`
- Normalised data MUST be stored in `s3://engaging-datalake/clean`
- Normalised data SHOULD BE pushed to a database for processing.

Normalised data MUST BE in (optionally gzipped) CSV format, with headers coding the fieldname and the expected type.


