import pandas as pd


def dataframe_diff(df1, df2, index):
    """
    Compare 2 pandas dataframe. Output a dataframe:
     - the rows in df2 that are not in df1
     - the rows in df2 that differs from the one in df1 for the same index.
     Both dataframe must share the same columns AND a primary index.
    :param df1:
    :param df2:
    :param index:
    :return:
    """
    d1 = df1.copy()
    d2 = df2.copy()

    d1.index = d1[index]
    d2.index = d2[index]
    d1.sort_index(inplace=True)
    d2.sort_index(inplace=True)

    union = pd.concat([d1[index],  d2[index]])  # concatenate both list of primary index
    new_index = union[~union.duplicated(keep=False)]  # Remove all the ones that appear more than once
    new_rows = d2.reindex(new_index)
    d2 = d2.loc[d2.index.intersection(d1.index)]
    d1 = d1.loc[d1.index.intersection(d2.index)]

    # Compare previous / new export, extract rows that differ
    is_diff = (d2.fillna(0) != d1.fillna(0)).any(1)  # .fillna, because NaN != NaN.
                                                     # .any(*1*) to look at diff in rows
    return pd.concat([d2.loc[is_diff[is_diff].index], new_rows])
