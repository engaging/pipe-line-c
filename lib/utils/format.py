import datetime
from lib.utils.types import is_type, to_date, to_type


DTYPES = {
    'bool': 'bool',
    'category': 'str',
    'object': 'str',
    'float64': 'float',
    'datetime64[ns]': 'datetime',
    'int8': 'int',
    'int64': 'int',

}


def convert_from_pandas(df, source=None):
    res = df.copy()
    res['_source!str'] = source
    res['_normalization_date!datetime'] = to_type(datetime.datetime.now(), 'datetime')

    dtypes = [(c, str(t)) for c, t in res.dtypes.iteritems()]
    res.rename(columns={c: "{}!{}".format(c, DTYPES[t]) for c, t in dtypes}, inplace=True)
    return res[sorted(res.columns)]
