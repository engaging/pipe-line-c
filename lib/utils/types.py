import numpy as np
import datetime
import dateutil.parser as parser
import ftfy
import re

MAX_STRING_BYTE_LENGTH = 65535


def is_type(v, type, nullable=True):
    assert type in ['str', 'float', 'int', 'date', 'time', 'datetime']

    if nullable and \
        (v is None or v is np.nan or
             ((isinstance(v, float) or isinstance(v, int)) and
                  np.isnan(v))):
        return True

    if type == 'str':
        return is_str(v)
    elif type == 'float':
        return is_float(v)
    elif type == 'int':
        return is_int(v)
    elif type == 'date':
        return is_date(v)
    elif type == 'time':
        return is_time(v)
    elif type == 'datetime':
        return is_datetime(v)

    return False


def to_type(v, type, nullable=True):
    if nullable and \
        (v is None or v is np.nan or
             ((isinstance(v, float) or isinstance(v, int)) and
                  np.isnan(v))):
        return v
    assert type in ['str', 'float', 'int', 'date', 'time', 'datetime']
    if type == 'str':
        return to_str(v)
    if type == 'int':
        return to_int(v)
    if type == 'float':
        return to_float(v)
    if type == 'date':
        return to_date(v)
    if type == 'time':
        return to_time(v)
    if type == 'datetime':
        return to_datetime(v)


plaintext_unicode_re = re.compile('[\u0020-\u007E\u00A0-\U0010FFFF]*')


def is_plaintext_unicode(string):
    return plaintext_unicode_re.fullmatch(string) is not None


def to_str(v):
    # Remove leading / trailing `"`, fix broken encoding, check that is valid utf8
    s = ftfy.fix_text(str(v).replace("\n", " ").strip('"').strip())
    assert is_plaintext_unicode(s), "Invalid Unicode: {!r}".format(str(v))
    assert len(s.encode('utf-8')) < MAX_STRING_BYTE_LENGTH
    return s


def to_int(v):
    try:
        if type(v) is int:
            return v
        if type(v) is float:
            return int(v)
        if type(v) is str:
            return int(v.replace(',', ''))
    except ValueError:
        raise ValueError("Can't convert value {!r} of type {} to int".format(v, type(v)))


def to_float(v):
    if type(v) is float:
        return v
    if type(v) is int:
        return float(v)
    if type(v) is str:
        return float(v.replace(',', ''))
    raise ValueError("Can't convert value {!r} of type {} to float".format(v, type(v)))


def to_date(v):
    if type(v) is datetime.date:
        return _str_date(v)
    if type(v) is datetime.datetime:
        return _str_date(v.date())
    if type(v) is str:
        return _str_date(parser.parse(v.replace('"', '')).date())
    raise ValueError("Can't convert value {!r} of type {} to date".format(v, type(v)))


def to_datetime(v):
    if type(v) is datetime.datetime:
        return _str_datetime(v)
    if type(v) is datetime.date:
        return _str_datetime(datetime.datetime.combine(v, datetime.time(0, 0)))
    if type(v) is str:
        return _str_datetime(parser.parse(v.replace('"', '')))
    raise ValueError("Can't convert value {!r} of type {} to datetime".format(v, type(v)))


def to_time(v):
    if type(v) is datetime.time:
        return _str_time(v)
    if type(v) is datetime.datetime:
        return _str_time(v.time())
    if type(v) is str:
        return _str_time(parser.parse(v.replace('"', '')).time())
    raise ValueError("Can't convert value {!r} of type {} to time".format(v, type(v)))


def _str_date(d):
    return d.strftime("%Y-%m-%d")


def _str_datetime(d):
    return d.strftime("%Y-%m-%d %H:%M:%S")


def _str_time(d):
    return d.strftime("%H:%M:%S")


# Various type testing: we coerce the value into the type and see if it works.
def is_str(v):
    try:
        str(v)
        return True
    except ValueError:
        return False


def is_float(v):
    try:
        to_float(v)
        return True
    except ValueError:
        return False


def is_int(v):
    try:
        to_int(v)
        return True
    except ValueError:
        return False


def is_date(v):
    try:
        to_date(v)
        return True
    except ValueError:
        return False


def is_time(v):
    try:
        to_time(v)
        return True
    except ValueError:
        return False


def is_datetime(v):
    try:
        to_datetime(v.strip())
        return True
    except ValueError:
        return False
