import re


def to_fieldname(field):
    """
    Strip illegal character, replace them with '_'
    :param field: field to sanitize
    :return: str, sanitized field
    """
    res = field.strip().lower()
    res = re.sub(r'\W', '_', res)
    assert len(res.replace('_', '')) > 0, "Empty sanitized field name {!r}".format(res)  # Not just '_'
    assert re.match(r'[a-z]', res), "Sanitized field name must start with a letter: {!r}".format(res)
    return res
