from partners.partner_pipeline import PartnerPipeline
import pandas as pd
from engagelib.ftp import ftp
import datetime
import logging, watchtower
import engagelib.aws.redshift as redshift
import tempfile
from lib.utils.dataframe_diff import dataframe_diff

import os
from partners.engagechicago import data_cleanup


class EngageDataPipeline(PartnerPipeline):
    name = 'engagechicago'
    long_name = "ENGAGE Data Ingestion Pipeline"
    """
    ENGAGE data comes from their FTP server. It's a password protected zip file we need to unzip then normalize
    """
    def __init__(self, ini='configs/engagechicago.ini', force_refresh=False):
        super().__init__(ini, force_refresh)
        self.ftp_src = ftp.FTP.from_config(self.ini, section='ftp')

    def sync_src_raw(self):
        """
        List the files that needs to be synchronized, then synchronize
        :return:
        """
        in_s3_src = self.ftp_src.list_files(self.cfg.get('ftp', 'path'))
        in_s3_raw = [f for f in self.s3.list_objects_names(self.cfg.get('pipeline', 'bucket_raw_prefix')) if f.endswith('/SUCCESS')]
        to_sync = [c for c in in_s3_src if c not in [self.name_raw_to_src(d) for d in in_s3_raw]]
        self.logger.info("{} src data to synchronize".format(len(to_sync)))
        for f in to_sync:
            self.src_to_raw(f)
        return to_sync

    def src_to_raw(self, f_src):
        """
        Copy data from ENGAGE's ftp server to our data lake.
        :param f_src:
        :return:
        """
        with tempfile.TemporaryDirectory() as tmpdir:
            try:
                self.is_valid_src_file(f_src)
            except (AssertionError, ValueError):
                self.logger.warning("Invalid file in FTP: {}. Ignored".format(f_src))

            ftp_fullpath = self.cfg.get('ftp', 'path') + f_src
            s3_fullpath = self.name_src_to_raw(f_src)
            outf = os.path.join(tmpdir, f_src)
            self.logger.debug("Uploading {} to {}".format(ftp_fullpath, s3_fullpath))
            self.ftp_src.download(ftp_fullpath, outf)
            self.s3.upload_file(outf, s3_fullpath)
            self.complete_src_to_raw(f_src.replace('.zip', ''))

    def name_raw_to_src(self, raw):
        return raw.replace(self.cfg.get('pipeline', 'bucket_raw_prefix'), '').replace('/SUCCESS', '') + '.zip'

    def name_src_to_raw(self, src):
        base = src.split('.')[0]
        return os.path.join(self.s3_raw_prefix, base, src)

    def sync_raw_norm(self):
        """
        Extract the list of files in the raw bucket, synchronize it with the normalized files.
        :return:
        """
        in_s3_raw = [f for f in self.s3.list_objects_names(self.s3_raw_prefix) if f.endswith('/SUCCESS')]
        in_s3_norm = [f
                      for f in self.s3.list_objects_names(self.s3_norm_prefix)
                      if f.endswith('/SUCCESS')]
        to_sync = [f for f in in_s3_raw if self.name_raw_to_norm(f) not in in_s3_norm]

        self.logger.info("{} data uploads to normalize".format(len(to_sync)))
        dirs_to_sync = [f.replace('/SUCCESS', '') for f in to_sync]
        for dir in dirs_to_sync:
            self.raw_to_norm(dir)
            self.updated = True

    def name_raw_to_norm(self, name):
        return name.replace(self.s3_raw_prefix, self.s3_norm_prefix)

    def raw_to_norm(self, dir):
        # The heavy lifting is done in another module.
        data_cleanup.normalize(dir, self.cfg, self.s3)
        self.complete_raw_to_norm(dir.split('/')[-1])

    def sync_to_redshift(self):
        from engagelib.aws.redshift.import_from_s3 import copy_from_s3
        # Get the most recent batch date
        last_batch = sorted([f.split('/')[-2] for f in self.s3.list_objects_names(self.s3_norm_prefix) if f.endswith('/SUCCESS')])[-1]
        self.logger.info("Most recent batch found: {}".format(last_batch))
        connection = self.get_redshift_connection()
        s3dir = self.cfg.get('pipeline', 'bucket_normalized_prefix', fallback='engagechicago/clean/v1/') + last_batch + '/'

        copy_from_s3(connection, self.s3, schema='import', iam_role=self.cfg.get('redshift', 'iam_role'),
                     table_prefix='engage', s3prefix=s3dir)

    def update_models(self):
        from partners.engagechicago import calculate_personas, calculate_opportunities
        self.logger.info("Calculating personas...")
        calculate_personas.run(config=self.ini)
        self.logger.info("Calculating opportunities...")
        calculate_opportunities.run(config=self.ini)
        if datetime.date.today().day == 2:
            self.logger.info("Monthly snapshot of opportunity / persona")
            connection = redshift.get_connection_from_config(self.ini)
            export_to = "{}/{}snapshots/engage_personas_{}.csv".format(
                self.cfg.get('pipeline', 's3_bucket'),
                self.cfg.get('pipeline', 'bucket_normalized_prefix'),
                datetime.date.today().isoformat())
            self.logger.info("Exporting personas to s3://{}".format(export_to))
            redshift.unload_to_s3(connection,
                                  iam_role=self.cfg.get('redshift', 'iam_role'),
                                  table='public.engage$persona',
                                  unload_to=export_to)

            export_to = "{}/{}snapshots/engage_opportunities_{}.csv".format(
                self.cfg.get('pipeline', 's3_bucket'),
                self.cfg.get('pipeline', 'bucket_normalized_prefix'),
                datetime.date.today().isoformat())
            self.logger.info("Exporting opportunities to s3://{}".format(export_to))
            redshift.unload_to_s3(connection,
                                  iam_role=self.cfg.get('redshift', 'iam_role'),
                                  table='public.engage$opportunity',
                                  unload_to=export_to)

    def post_refresh_hooks(self):
        self.logger.info("Running Capillory post-hook")
        self.upload_to_capillory()

    def upload_to_capillory(self):
        """ENGAGE specific: upload result of ETL to Capillory"""
        connection = redshift.get_sqlalchemy_connection_from_config(self.ini)
        self.logger.info("Pulling previous Capillory data")
        df_previous = pd.read_sql("SELECT * FROM engage$capillary_customers", connection)
        self.logger.info("Running Capillory ETL")
        redshift.run_etl('partners/engagechicago/etls/capillary_export.sql', connection=redshift.get_connection_from_config(self.ini))
        df_new = pd.read_sql("SELECT * FROM engage$capillary_customers", connection)

        df = dataframe_diff(df_previous, df_new, 'ccustomerid')
        with tempfile.TemporaryDirectory() as tmpdir:
            destination = os.path.join(tmpdir, 'engage_engaging_customers-{}.csv'.format(datetime.date.today().isoformat()))
            df.to_csv(destination, index=False)
            target_file = self.cfg.get('capillory', 'path') + '/' + os.path.basename(destination)
            self.logger.info("Uploading file {} to ftp://{}:{}{}. {} rows".format(
                destination,
                self.cfg.get('capillory', 'host'),
                self.cfg.get('capillory', 'port'),
                target_file,
                len(df)
            ))
            capillory_ftp = ftp.FTP.from_config(self.ini, section='capillory', passive=True)
            capillory_ftp.upload(destination, target_file)
            if datetime.date.today().weekday() == 5:  # Every Saturday morning
                self.logger.info("Weekly full batch export of capillory data")
                destination = os.path.join(tmpdir,
                                           'full_engage_engaging_customers-{}-{}-{}.csv'.format(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day)
                                           )
                df_new.to_csv(destination, index=False)
                target_file = self.cfg.get('capillory', 'path') + '/' + os.path.basename(destination)
                self.logger.info("Uploading file {} to ftp://{}:{}{}. {} rows".format(
                    destination,
                    self.cfg.get('capillory', 'host'),
                    self.cfg.get('capillory', 'port'),
                    target_file,
                    len(df)
                ))
                capillory_ftp.upload(destination, target_file)

            self.logger.info("Capillory upload done.")

    @staticmethod
    def is_valid_src_file(f):
        date, extension = f.split('.')
        assert len(date) == 8
        assert extension == 'zip'

        year = date[:4]
        month = date[4:6]
        day = date[6:8]
        try:
            datetime.date(int(year), int(month), int(day))
        except ValueError:
            raise ValueError("Invalid date for file {}".format(f))


if __name__ == '__main__':
    import configparser
    import boto3
    import datetime

    logging.basicConfig(level=os.environ.get('LOG_LEVEL', 'DEBUG'),
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename='last_run.log',
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    cfg = configparser.ConfigParser()
    ini = 'configs/engagechicago.ini'
    assert os.path.isfile(ini), "Can't find configuration file for ENGAGE Chicago: {!r}".format(ini)
    cfg.read(ini)

    # Log to CloudWatch
    cw = watchtower.CloudWatchLogHandler(
        log_group=cfg.get('aws', 'log_group'),
        stream_name=cfg.get('aws', 'stream_name') + '-' + datetime.datetime.now().isoformat().replace(':', '-'),
        create_log_group=False,
        boto3_session=boto3.Session(
            aws_access_key_id=cfg.get('aws', 'access_key'),
            aws_secret_access_key=cfg.get('aws', 'secret_key'),
            region_name=cfg.get('aws', 'region')
        )
    )
    cw.setLevel(logging.INFO)
    cw.setFormatter(
        logging.Formatter('[%(levelname)s] (%(asctime)s) %(process)d  %(name)s:%(funcName)s:%(lineno)s - %(message)s')
    )

    # add the handler to the root logger
    logging.getLogger('').addHandler(console)
    logging.getLogger('').addHandler(cw)

    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('urllib3').setLevel(logging.CRITICAL)
    logging.getLogger('s3transfer').setLevel(logging.CRITICAL)

    force_refresh = bool(os.environ.get('FORCE_REFRESH'))
    try:
        pipeline = EngageDataPipeline(ini='configs/engagechicago.ini', force_refresh=force_refresh)
        pipeline.run_all()
    except Exception as e:
        logging.fatal("Problem with pipeline: {!r}".format(e))
