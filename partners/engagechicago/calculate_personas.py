from sklearn.externals import joblib
import pandas as pd
from engagelib.aws.redshift import get_sqlalchemy_connection_from_config
from engagelib.aws.redshift.connection import get_connection_from_config
import tempfile
from engagelib.aws.s3 import S3
import os
import configparser
import logging


def run(config='configs/engagechicago.ini'):
    EXPORT_TABLE_NAME = 'import.engage$persona'
    PUBLIC_TABLE_NAME = 'public.engage$persona'

    cfg = configparser.ConfigParser()
    cfg.read(config)

    with tempfile.TemporaryDirectory() as tmpdir:
        s3_bucket = S3.from_config(bucket=cfg.get('models', 'bucket'), ini=config, inisection='aws')
        local_model_path = os.path.join(tmpdir, f'centroids.pkl')
        remote_path = cfg.get('models', 'persona_path')
        logging.debug(f"Downloading persona model from s3://{cfg.get('models', 'bucket')}/{remote_path} to {local_model_path}")
        s3_bucket.download_to_file(remote_path, local_model_path)
        clf = joblib.load(local_model_path)

    keeps = ['nb_purchase_value: habit building', 'nb_purchase_value: discount bargain hunter', 'nb_purchase_features: indulgence', 'nb_purchase_features: home',
             'nb_purchase_features: diet', 'nb_purchase_features: health', 'nb_purchase_pain relief: pain relief', 'nb_purchase_pain relief: full body',
             'nb_purchase_pain relief: upper body', 'nb_purchase_relaxation: light relaxation', 'nb_purchase_relaxation: head relax',
             'nb_purchase_size: space friendly', 'nb_purchase_size: portable', 'nb_purchase_technology: warmth', 'nb_purchase_technology: body detection',
             'nb_purchase_technology: human 3d tech', 'nb_purchase_technology: air bag', 'nb_purchase_usability: versatile usage',
             'nb_purchase_usability: also for senior', 'nb_purchase_usability: also for junior', 'nb_purchase_usability: build in caster',
             'nb_purchase_usability: removable cover ', 'nb_purchase_usability: battery', 'nb_purchase_usability: detachable part', 'nb_purchase_added value: music',
             'nb_purchase_added value: car adapter', 'nb_purchase_added value: dust bag', 'nb_purchase_added value: light',
             'nb_purchase_added value: massage expert endorment', 'nb_purchase_added value: artiste', 'nb_paid_baskets']

    connection = get_sqlalchemy_connection_from_config(config, 'redshift')
    query = """
        SELECT 
            customer_id,
            {}
        FROM public.engage$customer_item_attributes
    """.format(",\n\t".join(['"{}"'.format(c) for c in keeps]))
    df = pd.read_sql(query, connection)
    df['persona'] = clf.predict(df[keeps])
    df['persona'] = df['persona'].map(lambda x: 'ABCDEFGHI'[x])  # from digit to letter
    with tempfile.TemporaryDirectory() as tmpdir:
        bucket = S3.from_config('engaging-datalake', config, inisection='aws')
        f = os.path.join(tmpdir, 'persona.csv.gz')
        upload_to = 'engagechicago/clean/v1/alt/persona.csv.gz'
        df[['customer_id', 'persona']].to_csv(f, compression='gzip', index=False)
        bucket.upload_file(f, upload_to)

    connection = get_connection_from_config(config)

    query = f"""
    DROP TABLE IF EXISTS {EXPORT_TABLE_NAME};
    CREATE TABLE {EXPORT_TABLE_NAME} (
            "customer_id"   VARCHAR(65535),
            "persona"       VARCHAR(65535)
    );
    
    COPY {EXPORT_TABLE_NAME}
    FROM 's3://engaging-datalake/{upload_to}'
    iam_role '{cfg.get('redshift', 'iam_role')}'
    region 'ap-southeast-1'
    IGNOREHEADER 1
    CSV DELIMITER ','
    GZIP
    DATEFORMAT 'YYYY-MM-DD'
    TIMEFORMAT 'YYYY-MM-DD HH:MI:SS'
    EMPTYASNULL;
    
    DROP TABLE IF EXISTS {PUBLIC_TABLE_NAME};
    CREATE TABLE {PUBLIC_TABLE_NAME} AS (
      SELECT 
       c.customer_id,
       COALESCE(p.persona, '{cfg.get('models', 'default_persona')}') persona
      FROM 
       public.engage$customers c
      LEFT JOIN {EXPORT_TABLE_NAME} p
      ON c.customer_id = p.customer_id
    );
    
    
    COMMIT;
    """

    cursor = connection.cursor()
    logging.debug("Running query")
    logging.debug(query)
    cursor.execute(query)

    return df
