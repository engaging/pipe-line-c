import pandas as pd
import numpy as np
import datetime
import logging


logger = logging.getLogger(__name__)


def to_boolean(v):
    if v is None or v is np.nan:
        return v
    v = v.strip()
    assert v.upper() in ['Y', 'N'], "Invalid value for Boolean {!r}".format(v)
    return v.upper() == 'Y'


def parse_str_date(v):
    if v is None or v is np.nan or v.strip() == '' or v == "XXXXXXXX":
        return None, None, None
    year, month, day = v[:4], v[4:6], v[6:8]
    if len(v) == 14:
        hour, minute, second = v[8:10], v[10:12],  v[12:14]
    else:
        hour, minute, second = None, None, None

    try:
        year = int(year)
    except ValueError:
        year = None
    try:
        month = int(month)
    except ValueError:
        month = None
    try:
        day = int(day)
    except ValueError:
        day = None
    try:
        hour = int(hour)
    except (ValueError, TypeError):
        hour = None
    try:
        minute = int(minute)
    except (ValueError, TypeError):
        minute = None
    try:
        second = int(second)
    except (ValueError, TypeError):
        second = None

    return year, month, day, hour, minute, second


def str_to_datetime(v, fail_on_error=True):
    if v is None or v is np.nan:
        if fail_on_error:
            logging.fatal("Invalid value for datetime {!r}".format(v))
        return pd.NaT

    v = v.strip()
    if len(v) == 8:  # It's a date
        return datetime.datetime.combine(str_to_date(v, fail_on_error=fail_on_error), datetime.time())

    if len(v) != 14:
        m = "Invalid value for datetime {!r}".format(v)
        if fail_on_error:
            logger.fatal(m)
        else:
            logger.warning(m)
            return pd.NaT

    try:
        year, month, day, hour, minute, second = parse_str_date(v)
        return datetime.datetime(year, month, day, hour, minute, second)

    except ValueError:
        m = "Invalid value for datetime {!r}".format(v)
        if fail_on_error:
            logger.fatal(m)
        else:
            logger.warning(m)
            return pd.NaT


def str_to_date(v, fail_on_error=True):
    year, month, day, *_ = parse_str_date(v)
    if year is None or month is None or day is None:
        return pd.NaT
    else:
        try:
            return datetime.date(year, month, day)
        except ValueError:
            if fail_on_error:
                logger.fatal("Invalid date {!r}".format(v))
            else:
                logger.error("Invalid date {!r}".format(v))
                return pd.NaT


def to_stripped_str(v):
    if v is None or v is np.nan:
        return ''
    return str(v).strip()

