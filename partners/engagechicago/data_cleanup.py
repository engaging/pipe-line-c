import pandas as pd
import logging
import partners.engagechicago.types as t
import datetime
import numpy as np
from lib.utils.format import convert_from_pandas
import tempfile
import os
import zipfile


# This should really be done in SQL...
def normalize(batch, cfg, bucket):
    with tempfile.TemporaryDirectory() as tempdir:
        basename = batch.split('/')[-1] + '.zip'
        zf = batch + '/' + basename
        logging.debug("Downloading {}".format(zf))
        fout = os.path.join(tempdir, basename)
        bucket.download_to_file(zf, fout)
        logging.debug("Unzipping file...")
        zipfile.ZipFile(fout).extractall(path=tempdir, pwd=cfg.get('ftp', 'zip_password').encode())

        logging.info("Processing customers table")
        customers = pd.read_csv(os.path.join(tempdir, 'customer.csv'), dtype='str')
        customers['customer_zip'] = customers['PostalCode'].map(t.to_stripped_str)
        customers['customer_id'] = customers['CustomerID'].map(t.to_stripped_str)
        customers['customer_address_type'] = customers['AddressType'].map(t.to_stripped_str).astype('category')
        customers['customer_address_district'] = customers['AddressDistrict'].map(t.to_stripped_str)
        customers['customer_gender'] = customers['Gender'].astype('category')
        customers['customer_year_of_birth'] = customers['BirthDate'].map(lambda x: t.parse_str_date(x)[0])
        customers['customer_month_of_birth'] = customers['BirthDate'].map(lambda x: t.parse_str_date(x)[1])
        customers['customer_day_of_birth'] = customers['BirthDate'].map(lambda x: t.parse_str_date(x)[2])
        customers['customer_created_date'] = customers['CreatedDate'].map(t.str_to_datetime)
        customers['customer_has_email'] = customers['HasEmail'].map(t.to_boolean)
        customers['customer_has_contact'] = customers['HasContact'].map(t.to_boolean)
        customers['customer_allow_email'] = customers['AllowEmail'].map(t.to_boolean)
        customers['customer_allow_mail'] = customers['AllowMail'].map(t.to_boolean)
        customers['customer_allow_sms'] = customers['AllowSMS'].map(t.to_boolean)
        customers['customer_last_update'] = customers['LastUpdate'].map(t.str_to_datetime)
        customers = customers[['customer_zip', 'customer_id', 'customer_address_type', 'customer_address_district',
                               'customer_gender', 'customer_year_of_birth', 'customer_month_of_birth',
                               'customer_day_of_birth', 'customer_created_date', 'customer_has_email',
                               'customer_has_contact', 'customer_allow_email', 'customer_allow_mail',
                               'customer_allow_sms', 'customer_last_update']]

        logging.info("Processing items table")
        items = pd.read_csv(os.path.join(tempdir, 'item.csv'), dtype='str')

        items['item_no'] = items['ItemNo'].map(t.to_stripped_str)
        items['item_description'] = items['ItemDescription'].map(t.to_stripped_str)
        items['item_model_description'] = items['ItemModelDesc'].map(t.to_stripped_str)
        items['item_model'] = items['ItemModel'].map(t.to_stripped_str)
        items['item_type'] = items['ItemType'].map(t.to_stripped_str).astype('category')
        items['item_category'] = items['ItemCategory'].map(t.to_stripped_str).astype('category')
        items['item_group'] = items['ItemGroup'].map(t.to_stripped_str).astype('category')
        items = items[['item_no', 'item_description', 'item_model', 'item_model_description', 'item_type', 'item_category', 'item_group']]

        logging.info("Processing outlets table")
        outlets = pd.read_csv(os.path.join(tempdir, 'outlet.csv'), dtype='str')

        outlets['outlet_code'] = outlets['cOutletCode'].map(t.to_stripped_str)
        outlets['outlet_address'] = outlets['Address'].map(t.to_stripped_str)
        outlets['outlet_zip'] = outlets['PostalCode'].map(t.to_stripped_str)
        outlets['outlet_address_type'] = outlets['AddressType'].map(t.to_stripped_str).astype('category')
        outlets['outlet_address_district'] = outlets['AddressDistrict'].map(t.to_stripped_str).astype('category')
        outlets['outlet_type'] = outlets['cOutletType'].map(t.to_stripped_str).astype('category')
        outlets = outlets[['outlet_code', 'outlet_address', 'outlet_zip', 'outlet_address_type', 'outlet_address_district', 'outlet_type']]

        logging.info("Processing promo table")
        promos = pd.read_csv(os.path.join(tempdir, 'promo.csv'), dtype='str')

        promos['promo_id'] = promos['iPromoScheme'].map(t.to_stripped_str)
        promos['promo_description'] = promos['cDesc'].map(t.to_stripped_str)
        promos['promo_start_date'] = promos['cStartDate'].astype('str').map(t.str_to_date)
        promos['promo_end_date'] = promos['cEndDate'].astype('str').map(t.str_to_date)
        promos = promos[['promo_id', 'promo_description', 'promo_start_date', 'promo_end_date']]

        logging.info("Processing sales orders table")
        sales = pd.read_csv(os.path.join(tempdir, 'salesorder.csv'), dtype='str')

        sales['customer_id'] = sales['CustomerID'].map(t.to_stripped_str)
        sales['order_number'] = sales['SONumber'].map(t.to_stripped_str)
        sales['order_date'] = sales['SODate'].map(t.str_to_date)
        sales['created_datetime'] = sales['CreatedDateTime'].map(t.str_to_datetime)
        sales['outlet_code'] = sales['OutletCode'].map(t.to_stripped_str)
        sales['salesperson'] = sales['Salesman'].map(t.to_stripped_str)
        sales['line_number'] = sales['LineNo'].astype('int8')
        sales['item_no'] = sales['ItemNo'].map(t.to_stripped_str)
        sales['item_quantity'] = sales['Qty'].astype('int')
        sales['item_price'] = sales['Price'].astype('float')
        sales['item_discount'] = sales['Discount'].astype('float')
        sales['total_amount'] = sales['TotalAmt'].astype('float')
        sales['total_amount_with_tax'] = sales['TotalAmtWithTax'].astype('float')
        sales['promo_id'] = sales['PromoID'].map(t.to_stripped_str)
        sales['promo_line_no'] = sales['PromoLineNo'].map(t.to_stripped_str)
        sales['completed'] = sales['Completed'].map(t.to_boolean)
        sales['day_of_week'] = sales['order_date'].map(lambda x: x.weekday())

        # Check that there are no reversal transactions in the dataset.
        assert not sales['order_number'].map(lambda x: x.startswith('ASYS')).any()

        sales = sales[['customer_id', 'order_number', 'order_date', 'created_datetime', 'outlet_code', 'salesperson',
                       'line_number', 'item_no', 'item_quantity', 'item_price', 'item_discount', 'total_amount',
                       'total_amount_with_tax', 'promo_id', 'promo_line_no', 'completed']]

        # Date of first order
        first_order_date = sales[['customer_id', 'order_date']].groupby('customer_id').order_date.apply(min)
        first_order_date.name = 'first_order_date'

        customers = customers.join(first_order_date, how='left', on='customer_id')
        sales = sales.join(first_order_date, how='left', on='customer_id')
        sales['is_returning_customer'] = sales['order_date'] > sales['first_order_date']
        sales = sales.merge(items, how='left', left_on='item_no', right_on='item_no')
        sales = sales.merge(promos, how='left', left_on='promo_id', right_on='promo_id')
        sales = sales.merge(outlets, how='left', left_on='outlet_code', right_on='outlet_code')

        sales['is_trade_in'] = sales['item_type'] == 'TRADE IN'
        sales['nb_voucher_used'] = sales.apply(lambda row: -row.item_quantity if row.item_type == 'CORPORATE VOUCHER' else 0, axis=1)
        sales['nb_redemption_used'] = sales.apply(lambda row: -row.item_quantity if row.item_type == 'CORPORATE REDEMPTION' else 0, axis=1)
        sales['total_discount_from_voucher'] = sales.apply(lambda row: -row.total_amount_with_tax if row.item_type == 'CORPORATE VOUCHER' else 0, axis=1)
        sales['is_freebie'] = (sales['item_quantity'] > 0) & (sales['item_price'] == 0) & (sales['total_amount_with_tax'] == 0) & (sales['item_type'].isin(['PRODUCTS', 'ACCESSORIES'])) & (sales['promo_id'] == '0')
        sales['discount_pc'] = ((sales['item_price'] * sales['item_quantity']) - sales['total_amount_with_tax']) / (sales['item_price'] * sales['item_quantity'])
        sales['is_gwp'] = (sales['discount_pc'] == 1) & (~sales['promo_id'].isin(['1', '2', '3']))
        sales['is_paid_item'] = (sales['item_quantity'] > 0) & (sales['total_amount_with_tax'] > 0)
        sales['is_paid_product'] = (sales['item_quantity'] > 0) & (sales['total_amount_with_tax'] > 0) & ((sales['item_type'] == 'PRODUCTS') | (sales['item_type'] == 'ACCESSORIES'))
        sales['is_technical_service'] = sales['item_type'] == 'TSS'
        sales['is_warranty'] = sales['item_model'] == 'WARRANTY'
        sales['is_birthday_promotion'] = sales['promo_description'].map(lambda x: 'birthday customers' in x.lower() if x is not np.nan else False).astype('bool')

        sales = sales.sort_values(by='order_number')

        item_categories = [c for c in items.item_category.unique() if c != '']
        for cat in item_categories:
            sales['is_category {}'.format(cat.lower())] = sales['item_category'] == cat

        # Customer age
        current_year = datetime.date.today().year
        customers['customer_year_of_birth'] = customers['customer_year_of_birth'].map(lambda x: x if 1900 < x < current_year - 15 else None)
        customers['customer_age'] = customers['customer_year_of_birth'].map(lambda x: current_year - x if x is not None else None)

        # Membership
        customers['member_for_years'] = customers['customer_created_date'].map(lambda x: current_year - x.year)

        # CAC
        logging.info("Processing campaign cost attribution tables")
        campaigns = pd.read_csv(os.path.join(tempdir, 'campaign.csv'), dtype='str')
        campaigns.rename(columns={
            "CampaignID": 'campaign_id',
            "CampaignGroup": 'campaign_group',
            "Description": 'description',
            "Channel": 'channel',
            "StartDate": 'start_date',
            "EndDate": 'end_date',
            "LookbackDay": 'lookback',
            "Spending": 'spending',
            "ItemWeightage": 'item_weightage',
            "ExternalID": 'external_id'
        }, inplace=True)
        campaigns['start_date'] = campaigns['start_date'].map(t.str_to_date)
        campaigns['end_date'] = campaigns['end_date'].map(t.str_to_date)
        campaigns['lookback'] = campaigns['lookback'].astype('int')
        campaigns['spending'] = campaigns['spending'].astype('float')
        campaigns['item_weightage'] = campaigns['item_weightage'].astype('float')

        campaign_customers = pd.read_csv(os.path.join(tempdir, 'campaigncustomer.csv'), dtype='str')
        campaign_customers.rename(columns={
            "CampaignID": 'campaign_id',
            "CustomerID": 'customer_id'
        }, inplace=True)

        campaign_item_model = pd.read_csv(os.path.join(tempdir, 'campaignitemmodel.csv'), dtype='str')
        campaign_item_model.rename(columns={
            "CampaignID": 'campaign_id',
            "ItemModel": 'item_model'
        }, inplace=True)

        campaign_outlet = pd.read_csv(os.path.join(tempdir, 'campaignoutlet.csv'), dtype='str')
        campaign_outlet.rename(columns={
            "CampaignID": 'campaign_id',
            "OutletCode": 'outlet_code'
        }, inplace=True)

        campaign_promo = pd.read_csv(os.path.join(tempdir, 'campaignpromoscheme.csv'), dtype='str')
        campaign_promo.rename(columns={
            "CampaignID": 'campaign_id',
            "iPromoScheme": 'promo_id'
        }, inplace=True)

    with tempfile.TemporaryDirectory() as tempdir:
        logging.info("Loading to S3")
        s3dir = cfg.get('pipeline', 'bucket_normalized_prefix') + batch.split('/')[-1] + '/'

        file = 'sales.csv.gz'
        path = s3dir + file
        convert_from_pandas(sales, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'customers.csv.gz'
        path = s3dir + file
        convert_from_pandas(customers, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'promos.csv.gz'
        path = s3dir + file
        convert_from_pandas(promos, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'outlets.csv.gz'
        path = s3dir + file
        convert_from_pandas(outlets, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'items.csv.gz'
        path = s3dir + file
        convert_from_pandas(items, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'campaigns.csv.gz'
        path = s3dir + file
        convert_from_pandas(campaigns, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'campaign_customers.csv.gz'
        path = s3dir + file
        convert_from_pandas(campaign_customers, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'campaign_item_models.csv.gz'
        path = s3dir + file
        convert_from_pandas(campaign_item_model, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'campaign_outlet.csv.gz'
        path = s3dir + file
        convert_from_pandas(campaign_outlet, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        file = 'campaign_promo.csv.gz'
        path = s3dir + file
        convert_from_pandas(campaign_promo, source=path).to_csv(os.path.join(tempdir, file), index=False, compression='gzip')
        bucket.upload_file(os.path.join(tempdir, file), path)

        bucket.put_object(obj=b'', s3path=s3dir + 'SUCCESS')

    return sales, customers, items, outlets, promos, campaigns, campaign_customers, campaign_item_model


if __name__ == '__main__':
    LOG_LEVEL = 'DEBUG'
    logger = logging.getLogger(__name__)

    # Output to tty
    logging.basicConfig(
        # format='%(asctime)s %(levelname)s: %(message)s',
        level=LOG_LEVEL,
        datefmt='%m/%d/%Y %I:%M:%S %p'
    )

    logger.setLevel(LOG_LEVEL)
    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('urllib3').setLevel(logging.CRITICAL)
    logging.getLogger('s3transfer').setLevel(logging.CRITICAL)

    sales, customers, items, outlets, promos = normalize(load_into_redshift=True)
