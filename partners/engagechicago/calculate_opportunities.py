"""
Calculate category-level opportunity based on the models in partners/engagechicago/models/*
"""
import pandas as pd
from engagelib.aws.redshift import get_sqlalchemy_connection_from_config
from engagelib.aws.s3 import S3
from sklearn.externals import joblib
import configparser
import logging
import os
import time
import tempfile

logger = logging.getLogger('pipeline.engage.calculate_opportunities')


def run(config='configs/engagechicago.ini'):
    connection = get_sqlalchemy_connection_from_config(config)

    categories = [
        ('chair', 'Massage Chair'),
        ('sofa', 'Massage Sofa'),
        ('leg_massager', 'Leg Massager'),
        ('upper_body', 'Upper Body Massager'),
        ('air_purifier', 'Air Purifier'),
        ('humidifier', 'Humidifier'),
        ('eye_massager', 'Eye Massager'),
        ('handheld_massager', 'Handheld Massager')
    ]

    no_rebuy = {
        'chair': 3*365,
        'sofa': 3*365,
        'leg_massager': 2.95*365,
        'upper_body': 2.98*365,
        'air_purifier': 2.95*365,
        'humidifier': 2.98*365,
        'eye_massager': 2.95*365,
        'handheld_massager': 2.95*365
    }

    opportunities = {}

    cfg = configparser.ConfigParser()
    cfg.read(config)

    s3_bucket = S3.from_config(bucket=cfg.get('models', 'bucket'), ini=config, inisection='aws')

    # Get the current period. More or less future proof.
    current_period = connection.execute("SELECT period_index FROM engage$grid_base "
                                        "WHERE current_date > date_begin AND current_date <= date_end LIMIT 1").first()[0]
    current_period = 17

    # Check which fields we need, so we only pull those from the database, for memory concerns
    df = pd.read_sql("SELECT * FROM engage$customer_grid LIMIT 1", connection)
    xs = [c for c in df.columns if not c.startswith('z_') and
          not c.startswith('y_') and
          not c.startswith('cat_')]

    assert current_period is not None and type(current_period) is int and current_period >= 16
    all_fields = ",".join(['"{}"'.format(f) for f in xs + ['z_customer_id']])

    # There is a dirty trick here, where we replace a field with {field}, so next time we .format() we can use that
    # field again (_placeholder)
    query_template = """
SELECT {all_fields} 
FROM engage$customer_grid c 
WHERE z_period_index = {current_period} 
AND ("time_since_last_purchase_{category_placeholder}" > {no_rebuy}
OR "time_since_last_purchase_{category_placeholder}" = -1)
AND z_customer_id NOT IN (
  SELECT DISTINCT(customer_id) 
  FROM engage$product_sold 
  WHERE item_category = '{category_long_placeholder}'
  AND order_date >= current_date - 190 
  )
""".format(all_fields=all_fields, current_period=current_period, no_rebuy='{no_rebuy}',
           category_placeholder='{category}', category_long_placeholder='{category_long}')

    with tempfile.TemporaryDirectory() as tmpdir:
        for category, category_long in categories:
            logger.debug("Calculating opportunity for category {}".format(category))
            query = query_template.format(category=category, category_long=category_long, no_rebuy=no_rebuy[category])
            logger.debug(query)
            begin = time.time()
            population = pd.read_sql(query, connection)
            assert len(population) > 0, "No customers selected for opportunity calculation!"
            logger.debug("\tPulled {} rows in {:.2f}s".format(len(population), time.time()-begin))
            population.index = population['z_customer_id']

            # Load model

            local_model_path = os.path.join(tmpdir, f'opportunity_{category}.pkl')
            remote_path = cfg.get('models', 'opportunity_path') + f'opportunity_{category}.pkl'
            logger.debug(f"Downloading models from {remote_path} to {local_model_path}")
            s3_bucket.download_to_file(remote_path, local_model_path)
            clf = joblib.load(local_model_path)
            population['prediction'] = clf.predict(population[xs])

            for customer_id, opportunity in population['prediction'].iteritems():
                if customer_id not in opportunities and opportunity == 1:
                    opportunities[customer_id] = category
            logger.debug("{} opportunities recorded".format(len(opportunities)))
            del population

        target_table = cfg.get('models', 'opportunity_table')
        target_schema = cfg.get('models', 'opportunity_schema', fallback='public')

        logger.info("Saving opportunities to database... ({} rows)".format(len(opportunities)))
        res = pd.DataFrame(pd.Series(opportunities), columns=['opportunity'])
        res['customer_id'] = res.index
        res[['customer_id', 'opportunity']].to_sql(con=connection, name=target_table, schema=target_schema, index=False,
                                                   if_exists='replace')


if __name__ == '__main__':
    run()
