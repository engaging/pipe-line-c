-- 040-customer-item-attribute.sql
-- set item attributes at the customer level, for persona calculation.


DROP TABLE IF EXISTS public.engage$customer_item_attributes;
CREATE TABLE public.engage$customer_item_attributes AS (
    SELECT
      s.customer_id,

      COALESCE(SUM("value: habit building"), 0) "nb_purchase_value: habit building",
      COALESCE(SUM("value: special occasion"), 0) "nb_purchase_value: special occasion",
      COALESCE(SUM("value: discount bargain hunter"), 0) "nb_purchase_value: discount bargain hunter",
      COALESCE(SUM("value: luxury"), 0) "nb_purchase_value: luxury",
      COALESCE(SUM("features: indulgence"), 0) "nb_purchase_features: indulgence",
      COALESCE(SUM("features: office"), 0) "nb_purchase_features: office",
      COALESCE(SUM("features: home"), 0) "nb_purchase_features: home",
      COALESCE(SUM("features: travel"), 0) "nb_purchase_features: travel",
      COALESCE(SUM("features: athlete"), 0) "nb_purchase_features: athlete",
      COALESCE(SUM("features: beauty"), 0) "nb_purchase_features: beauty",
      COALESCE(SUM("features: diet"), 0) "nb_purchase_features: diet",
      COALESCE(SUM("features: health"), 0) "nb_purchase_features: health",
      COALESCE(SUM("features: children oriented"), 0) "nb_purchase_features: children oriented",
      COALESCE(SUM("pain relief: pain relief"), 0) "nb_purchase_pain relief: pain relief",
      COALESCE(SUM("pain relief: full body"), 0) "nb_purchase_pain relief: full body",
      COALESCE(SUM("pain relief: head"), 0) "nb_purchase_pain relief: head",
      COALESCE(SUM("pain relief: upper body"), 0) "nb_purchase_pain relief: upper body",
      COALESCE(SUM("pain relief: lower body"), 0) "nb_purchase_pain relief: lower body",
      COALESCE(SUM("pain relief: foot & leg"), 0) "nb_purchase_pain relief: foot & leg",
      COALESCE(SUM("pain relief: extra head focus"), 0) "nb_purchase_pain relief: extra head focus",
      COALESCE(SUM("relaxation: light relaxation"), 0) "nb_purchase_relaxation: light relaxation",
      COALESCE(SUM("relaxation: full body"), 0) "nb_purchase_relaxation: full body",
      COALESCE(SUM("relaxation: facial relax"), 0) "nb_purchase_relaxation: facial relax",
      COALESCE(SUM("relaxation: lower body"), 0) "nb_purchase_relaxation: lower body",
      COALESCE(SUM("relaxation: head relax"), 0) "nb_purchase_relaxation: head relax",
      COALESCE(SUM("relaxation: eye relax"), 0) "nb_purchase_relaxation: eye relax",
      COALESCE(SUM("relaxation: hand"), 0) "nb_purchase_relaxation: hand",
      COALESCE(SUM("relaxation: lumbar support"), 0) "nb_purchase_relaxation: lumbar support",
      COALESCE(SUM("pricing: high"), 0) "nb_purchase_pricing: high",
      COALESCE(SUM("pricing: mid"), 0) "nb_purchase_pricing: mid",
      COALESCE(SUM("pricing: low"), 0) "nb_purchase_pricing: low",
      COALESCE(SUM("size: space consuming"), 0) "nb_purchase_size: space consuming",
      COALESCE(SUM("size: space friendly"), 0) "nb_purchase_size: space friendly",
      COALESCE(SUM("size: portable"), 0) "nb_purchase_size: portable",
      COALESCE(SUM("technology: warmth"), 0) "nb_purchase_technology: warmth",
      COALESCE(SUM("technology: body detection"), 0) "nb_purchase_technology: body detection",
      COALESCE(SUM("technology: tui na"), 0) "nb_purchase_technology: tui na",
      COALESCE(SUM("technology: gua sha"), 0) "nb_purchase_technology: gua sha",
      COALESCE(SUM("technology: twin power engageotion"), 0) "nb_purchase_technology: twin power engageotion",
      COALESCE(SUM("technology: hybrid power-ball"), 0) "nb_purchase_technology: hybrid power-ball",
      COALESCE(SUM("technology: s-care roller"), 0) "nb_purchase_technology: s-care roller",
      COALESCE(SUM("technology: human 3d tech"), 0) "nb_purchase_technology: human 3d tech",
      COALESCE(SUM("technology: v-hand tech"), 0) "nb_purchase_technology: v-hand tech",
      COALESCE(SUM("technology: zero-gravity"), 0) "nb_purchase_technology: zero-gravity",
      COALESCE(SUM("technology: ion"), 0) "nb_purchase_technology: ion",
      COALESCE(SUM("technology: magnetic node"), 0) "nb_purchase_technology: magnetic node",
      COALESCE(SUM("technology: bio-energy"), 0) "nb_purchase_technology: bio-energy",
      COALESCE(SUM("technology: hand-powered"), 0) "nb_purchase_technology: hand-powered",
      COALESCE(SUM("technology: air bag"), 0) "nb_purchase_technology: air bag",
      COALESCE(SUM("usability: versatile usage"), 0) "nb_purchase_usability: versatile usage",
      COALESCE(SUM("usability: extendible foot massage"), 0) "nb_purchase_usability: extendible foot massage",
      COALESCE(SUM("usability: also for senior"), 0) "nb_purchase_usability: also for senior",
      COALESCE(SUM("usability: also for junior"), 0) "nb_purchase_usability: also for junior",
      COALESCE(SUM("usability: quick function"), 0) "nb_purchase_usability: quick function",
      COALESCE(SUM("usability: build in caster"), 0) "nb_purchase_usability: build in caster",
      COALESCE(SUM("usability: lightweight in category"), 0) "nb_purchase_usability: lightweight in category",
      COALESCE(SUM("usability: power efficiency"), 0) "nb_purchase_usability: power efficiency",
      COALESCE(SUM("usability: safety"), 0) "nb_purchase_usability: safety",
      COALESCE(SUM("usability: removable cover "), 0) "nb_purchase_usability: removable cover ",
      COALESCE(SUM("usability: battery"), 0) "nb_purchase_usability: battery",
      COALESCE(SUM("usability: detachable part"), 0) "nb_purchase_usability: detachable part",
      COALESCE(SUM("usability: wireless remote control"), 0) "nb_purchase_usability: wireless remote control",
      COALESCE(SUM("added value: music"), 0) "nb_purchase_added value: music",
      COALESCE(SUM("added value: app"), 0) "nb_purchase_added value: app",
      COALESCE(SUM("added value: special edition"), 0) "nb_purchase_added value: special edition",
      COALESCE(SUM("added value: user memories"), 0) "nb_purchase_added value: user memories",
      COALESCE(SUM("added value: car adapter"), 0) "nb_purchase_added value: car adapter",
      COALESCE(SUM("added value: dust bag"), 0) "nb_purchase_added value: dust bag",
      COALESCE(SUM("added value: light"), 0) "nb_purchase_added value: light",
      COALESCE(SUM("added value: massage expert endorment"), 0) "nb_purchase_added value: massage expert endorment",
      COALESCE(SUM("added value: artiste"), 0) "nb_purchase_added value: artiste",
      COALESCE(SUM("added value: design award"), 0) "nb_purchase_added value: design award",
      COALESCE(SUM("added value: dual-user"), 0) "nb_purchase_added value: dual-user",

      COALESCE(MAX(c.nb_paid_baskets), 0) "nb_paid_baskets",
      COALESCE(MAX(c.nb_close_to_birthday), 0) "nb_baskets_close_to_birthday",
      COALESCE(MAX(c.nb_motherday_purchase), 0) "nb_motherday_purchase"

    FROM engage$product_sold s
    LEFT JOIN import.engage$item_attributes att
    ON s.item_model = att.item_model

    LEFT JOIN public.engage$customers c
    ON s.customer_id = c.customer_id

    GROUP BY s.customer_id
);
