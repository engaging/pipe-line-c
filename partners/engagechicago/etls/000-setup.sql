-- wrapper for python's `assert` statement
CREATE OR REPLACE FUNCTION assert (test boolean, message text)
  RETURNS BOOLEAN
STABLE
AS $$
  assert test, message
  return True
$$ language plpythonu;
