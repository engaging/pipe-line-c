
DROP TABLE IF EXISTS _initial_revenue;
CREATE TEMPORARY TABLE _initial_revenue AS (
  SELECT
    customer_id,
    extract(year from order_date) order_year,
    sum(total) total
  FROM engage$baskets
  WHERE is_returning_customer is FALSE
  GROUP BY 1, 2, 3
);


DROP TABLE IF EXISTS _deferred_revenue;
CREATE TEMPORARY TABLE _deferred_revenue AS (
  SELECT
    b.customer_id,
    extract(year from b.customer_first_order_date) year_of_initial_order,
    extract(year from order_date) order_year,
    sum(total) total
  FROM engage$baskets b
  WHERE is_returning_customer is TRUE
  GROUP BY 1, 2, 3
);


DROP TABLE IF EXISTS engage$deferred_revenue;
CREATE TABLE engage$deferred_revenue AS (
    SELECT
      x.persona,
      x.order_year initial_year,
      x.total initial_revenue,
      y.order_year deferred_year,
      y.total deferred_revenue,
      y.total / x.total ratio_deferred
    FROM
    (SELECT
      persona,
      order_year,
      sum(total) total
      FROM _initial_revenue i
      LEFT JOIN engage$persona p on p.customer_id = i.customer_id
      GROUP BY persona, order_year
    ) initial
    LEFT JOIN (
      SELECT
        p.persona,
        year_of_initial_order,
        order_year,
        sum(total) total
      FROM _deferred_revenue d
      LEFT JOIN engage$persona p on p.customer_id = d.customer_id
      GROUP BY 1, 2, 3
  ) deferred

  ON initial.order_year = deferred.year_of_initial_order and initial.persona = deferred.persona
  WHERE initial.persona IS NOT NULL
);


DROP TABLE IF EXISTS engage$three_year_deferred_revenue;
CREATE TABLE engage$three_year_deferred_revenue AS (
  SELECT
    x.persona,
    x.initial_year,
    x.initial_revenue,
    x.total_deferred_revenue,
    x.ratio_deferred_revenue

  FROM (
    SELECT
      persona,
      initial_year,
      initial_revenue,
      sum(deferred_revenue)                   total_deferred_revenue,
      sum(deferred_revenue) / initial_revenue ratio_deferred_revenue
    FROM engage$deferred_revenue
    WHERE initial_year < 2015
          AND deferred_revenue >= initial_year + 3
    GROUP BY persona, initial_year, initial_revenue
    ORDER BY persona, initial_year
  ) x
);
