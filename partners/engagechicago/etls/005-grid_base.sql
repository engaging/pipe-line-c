-- Creates a time grid on which the opportunity model aligns.
-- Also used in capillary_export.sql

DROP TABLE IF EXISTS engage$grid_base;
CREATE TABLE engage$grid_base (
  date_begin   DATE        NOT NULL,
  date_end     DATE        NOT NULL,
  period_name  VARCHAR(20) NOT NULL,
  period_index INT8       NOT NULL
);


INSERT INTO engage$grid_base VALUES

  (DATE '2010-07-01', DATE '2010-12-31', '2010 second half', 1),

  (DATE '2011-01-01', DATE '2011-06-30', '2011 first half', 2),
  (DATE '2011-07-01', DATE '2011-12-31', '2011 second half', 3),

  (DATE '2012-01-01', DATE '2012-06-30', '2012 first half', 4),
  (DATE '2012-07-01', DATE '2012-12-31', '2012 second half', 5),

  (DATE '2013-01-01', DATE '2013-06-30', '2013 first half', 6),
  (DATE '2013-07-01', DATE '2013-12-31', '2013 second half', 7),

  (DATE '2014-01-01', DATE '2014-06-30', '2014 first half', 8),
  (DATE '2014-07-01', DATE '2014-12-31', '2014 second half', 9),

  (DATE '2015-01-01', DATE '2015-06-30', '2015 first half', 10),
  (DATE '2015-07-01', DATE '2015-12-31', '2015 second half', 11),

  (DATE '2016-01-01', DATE '2016-06-30', '2016 first half', 12),
  (DATE '2016-07-01', DATE '2016-12-31', '2016 second half', 13),

  (DATE '2017-01-01', DATE '2017-06-30', '2017 first half', 14),
  (DATE '2017-07-01', DATE '2017-12-31', '2017 second half', 15),

  (DATE '2018-01-01', DATE '2018-06-30', '2018 first half', 16),
  (DATE '2018-07-01', DATE '2018-12-31', '2018 second half', 17),

  (DATE '2019-01-01', DATE '2019-06-30', '2019 first half', 18),
  (DATE '2019-07-01', DATE '2019-12-31', '2019 second half', 19),

  (DATE '2020-01-01', DATE '2020-06-30', '2020 first half', 20),
  (DATE '2020-07-01', DATE '2020-12-31', '2020 second half', 21),

  (DATE '2021-01-01', DATE '2021-06-30', '2021 first half', 22),
  (DATE '2021-07-01', DATE '2021-12-31', '2021 second half', 23),

  (DATE '2022-01-01', DATE '2022-06-30', '2022 first half', 24),
  (DATE '2022-07-01', DATE '2022-12-31', '2022 second half', 25),

  (DATE '2023-01-01', DATE '2023-06-30', '2023 first half', 26),
  (DATE '2023-07-01', DATE '2023-12-31', '2023 second half', 27),

  (DATE '2024-01-01', DATE '2024-06-30', '2024 first half', 28),
  (DATE '2024-07-01', DATE '2024-12-31', '2024 second half', 29),

  (DATE '2025-01-01', DATE '2025-06-30', '2025 first half', 30),
  (DATE '2025-07-01', DATE '2025-12-31', '2025 second half', 31)
; -- _grid_base

