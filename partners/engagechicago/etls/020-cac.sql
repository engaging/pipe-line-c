-- 020-cac.sql
-- Campaign Costs calculation, at the campaign, order and customer level.

-- from import
DROP TABLE IF EXISTS public.engage$campaigns;
CREATE TABLE public.engage$campaigns AS (
    SELECT
      campaign_group,
      campaign_id,
      channel,
      description,
      date(end_date) end_date,
      external_id,
      item_weightage::FLOAT,
      lookback::INT,
      spending::FLOAT,
      date(start_date) start_date
    FROM import.engage$campaigns
);

----------------------------------------------------
-- Some not completely related tables we need
----------------------------------------------------
DROP TABLE IF EXISTS public.engage$items_model_category;
CREATE TABLE public.engage$items_model_category AS (
    SELECT
      item_model,
      item_model_description,
      item_category,
      item_type
    FROM (
      SELECT
        item_model,
        item_model_description,
        item_category,
        item_type,
        row_number() OVER (PARTITION BY item_model) pos
      FROM
        public.engage$items
  ) c
  WHERE pos = 1
);

DROP TABLE IF EXISTS engage$basket_lite;
CREATE TABLE public.engage$basket_lite AS (
    SELECT
      order_id,
      customer_id,
      is_returning_customer,
      order_date,
      total,
      nb_paid_items,
      main_item_model,
      main_item_category
  FROM engage$baskets
);

SELECT assert(
    (
      SELECT count(*) FROM public.engage$items_model_category) = ((select count(distinct item_model) from public.engage$items)),
      'Duplicate/missing item models in items_model_category'
);

DROP TABLE IF EXISTS public.engage$campaign_item_models;
CREATE TABLE public.engage$campaign_item_models AS (
    SELECT
      c.campaign_id,
      c.item_model,
      i.item_model_description,
      i.item_category
    FROM import.engage$campaign_item_models c
  LEFT JOIN public.engage$items_model_category i
      on i.item_model = c.item_model
);

-- Assert that each campaign has at least one associated product
SELECT
  assert(COUNT(im.item_model) > 0, 'Campaign has no defined item model: '|| c.campaign_id)
FROM engage$campaigns c
LEFT JOIN engage$campaign_item_models im
  ON c.campaign_id = im.campaign_id
WHERE c.item_weightage > 0
GROUP BY c.campaign_id;

-- Assert each campaign item model is linked to a campaign
SELECT
  assert(c.campaign_id IS NOT NULL, 'campaign_item_model row has no corresponding campaign_id in `engage$campaigns`: ' || im.campaign_id)
    FROM engage$campaign_item_models im
    LEFT JOIN engage$campaigns c
      ON im.campaign_id = c.campaign_id;

-- End date must be after start date
SELECT
  assert(c.start_date <= c.end_date, 'End date before start date in campaign '||c.campaign_id)
FROM engage$campaigns c;

------------------------------------------------------------------------------------------------------------------------
-- ORDER LEVEL                                                                                                        --
------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS _campaign_order_item_link;
CREATE TEMPORARY TABLE _campaign_order_item_link AS (
    SELECT campaign_id, campaign_group, channel, item_model, total_amount_with_tax, customer_id, order_id, order_date FROM (
      SELECT campaign_id, campaign_group, channel, item_model, total_amount_with_tax, customer_id, order_id, order_date,
        row_number() OVER (PARTITION BY campaign_id, order_id ORDER BY total_amount_with_tax DESC) pos
      FROM (
             SELECT
               c.campaign_id,
               c.campaign_group,
               c.channel,
               im.item_model                 item_model,
               -- if a basket contains two "item" basket, that basket is counted twice. We don't want that. We select the most expensive one through the outer queries
               MAX(ts.total_amount_with_tax) total_amount_with_tax,
               ts.customer_id,
               ts.order_date,
               ts.order_id
             FROM engage$campaigns c
               LEFT JOIN engage$campaign_item_models im
                 ON im.campaign_id = c.campaign_id
               LEFT JOIN engage$transactions ts
                 ON ts.item_model = im.item_model AND ts.order_date BETWEEN c.start_date AND (c.end_date + c.lookback)
               LEFT JOIN engage$baskets b
                 ON b.order_id = ts.order_id
             WHERE ts.customer_id IS NOT NULL AND ts.total_amount_with_tax > 0 AND b.total > 0
             GROUP BY c.campaign_id, c.campaign_group, c.channel, ts.customer_id, ts.order_date, ts.order_id,
               im.item_model
           ) x
    ) y WHERE pos = 1
);


DROP TABLE IF EXISTS _campaign_order_brand_link;
CREATE TEMPORARY TABLE _campaign_order_brand_link AS (
  SELECT
    c.campaign_id,
    c.campaign_group,
    c.channel,
    b.main_item_model item_model, -- in that case, we can just group by main_item_model, as it is unique per basket and is faster
    b.customer_id,
    b.order_date,
    b.order_id
  FROM engage$campaigns c
    LEFT JOIN engage$campaign_item_models im
      ON im.campaign_id = c.campaign_id
    LEFT JOIN engage$baskets b
      ON b.order_date BETWEEN c.start_date AND (c.end_date + c.lookback)
    LEFT JOIN _campaign_order_item_link il
      ON il.order_id = b.order_id AND il.campaign_id = c.campaign_id
  WHERE il.order_id IS NULL -- remove rows already attributed to `item`
    AND b.order_id IS NOT NULL -- make sure we have at least one customer/basket
    AND b.total > 0
  GROUP BY c.campaign_id, c.campaign_group, c.channel, b.customer_id, b.order_date, b.order_id, b.main_item_model
);


DROP TABLE IF EXISTS engage$campaign_item_cost_per_order;
CREATE TABLE engage$campaign_item_cost_per_order AS (
  SELECT
    il.campaign_id,
    COUNT(DISTINCT il.order_id) nb_baskets,
    MAX(c.spending) total_campaign_cost,
    (MAX(c.item_weightage) * MAX(c.spending)) / COUNT(DISTINCT il.order_id) cost_per_order
  FROM _campaign_order_item_link il
  LEFT JOIN engage$campaigns c
    ON c.campaign_id = il.campaign_id
  GROUP BY c.campaign_id, il.campaign_id
);

DROP TABLE IF EXISTS engage$campaign_brand_cost_per_order;
CREATE TABLE engage$campaign_brand_cost_per_order AS (
  SELECT
    bl.campaign_id,
    COUNT(DISTINCT bl.order_id) nb_customer,
    MAX(c.spending) total_campaign_cost,
    ((1 - MAX(c.item_weightage)) * MAX(c.spending)) / COUNT(DISTINCT bl.order_id) cost_per_order
  FROM _campaign_order_brand_link bl
  LEFT JOIN engage$campaigns c
    ON c.campaign_id = bl.campaign_id
  GROUP BY bl.campaign_id, c.campaign_id
);

DROP TABLE IF EXISTS engage$campaign_order_cost;
CREATE TABLE engage$campaign_order_cost AS (
    SELECT x.*, i.item_category
    FROM
      ((
    SELECT
      c.campaign_id,
      c.campaign_group,
      c.channel,
      c.item_model,
      c.order_id,
      c.order_date,
      c.customer_id,
      ic.cost_per_order cost,
      cs.start_date,
      cs.end_date,
      'item' cost_attribution
    FROM _campaign_order_item_link c
    LEFT JOIN engage$campaigns cs
      ON cs.campaign_id = c.campaign_id
    LEFT JOIN engage$campaign_item_cost_per_order ic
      ON c.campaign_id = ic.campaign_id
  )
    UNION ALL  -- ALL is important
  (
    SELECT
      c.campaign_id,
      c.campaign_group,
      c.channel,
      c.item_model,
      c.order_id,
      c.order_date,
      c.customer_id,
      ic.cost_per_order cost,
      cs.start_date,
      cs.end_date,
      'brand' cost_attribution
    FROM _campaign_order_brand_link c
    LEFT JOIN engage$campaigns cs
      ON cs.campaign_id = c.campaign_id
    LEFT JOIN engage$campaign_brand_cost_per_order ic
      ON c.campaign_id = ic.campaign_id
  )) x
  LEFT JOIN (SELECT item_model, max(item_category) item_category FROM engage$items GROUP BY item_model) i
    ON x.item_model = i.item_model
);

DROP TABLE IF EXISTS engage$order_marketing_cost;
CREATE TABLE engage$order_marketing_cost AS (
    SELECT
      c.order_id,
      b.customer_id,
      c.total_marketing_cost,
      b.total
    FROM
      (SELECT
         c.order_id,
         SUM(c.cost) total_marketing_cost
       FROM engage$campaign_order_cost c
       GROUP BY c.order_id
      ) c
    LEFT JOIN engage$baskets b
      ON b.order_id = c.order_id
);

-- test that sum of cost = spending of campaign
SELECT
  SUM(cc.cost),
  MAX(c.spending),
  assert(
      abs(SUM(cc.cost) - MAX(c.spending)) < 0.001,
      'sum of cost doesn''t match campaign spending: (' || c.campaign_id || ') ' || SUM(cc.cost) || ' !=' || MAX(c.spending)
  )
FROM engage$campaign_order_cost cc
LEFT JOIN public.engage$campaigns c
  ON c.campaign_id = cc.campaign_id
GROUP BY c.campaign_id, cc.campaign_id
;

SELECT
  assert(
      abs((SELECT SUM(total_marketing_cost) FROM engage$order_marketing_cost) - (SELECT SUM(spending) FROM engage$campaigns)) < 0.0001,
      'sum of customer marketing cost != total marketing expense ' ||
      (SELECT SUM(total_marketing_cost) FROM engage$order_marketing_cost) || ' != ' ||
      (SELECT SUM(spending) FROM engage$campaigns)
  );

-- Test that the split of item/brand reconciles in the end.
SELECT
  assert(
  CASE WHEN cost_attribution = 'item'
  THEN
    abs(total_cost - cs.item_weightage * cs.spending) < 0.01
  ELSE
    abs(total_cost - (1-cs.item_weightage) * cs.spending) < 0.01
  END, 'Mismatch in distribution of item/brand budgets'
  )
FROM (
       SELECT
         campaign_id,
         channel,
         cost_attribution,
         SUM(cost) total_cost
       FROM
         engage$campaign_order_cost
       GROUP BY campaign_id, channel, cost_attribution
     ) c
  LEFT JOIN engage$campaigns cs
  ON cs.campaign_id = c.campaign_id;


DROP TABLE IF EXISTS engage$campaign_spending_per_model_per_day;
CREATE TABLE engage$campaign_spending_per_model_per_day AS (
  SELECT
    c.order_date date,
    c.item_category,
    c.item_model,
    c.campaign_id,
    c.persona,
    SUM(c.spending) spending
  FROM (
    SELECT
      oc.item_model,
      oc.item_category,
      oc.campaign_id,
      oc.order_date,
      pers.persona,
      SUM(oc.cost) spending
    FROM
      engage$campaign_order_cost oc
    LEFT JOIN engage$persona pers
      ON pers.customer_id = oc.customer_id
    WHERE oc.cost_attribution = 'item'
    GROUP BY 1, 2, 3, 4, 5
  ) c
  GROUP BY 1, 2, 3, 4, 5
);


DROP TABLE IF EXISTS engage$revenue_per_item_model_per_day;
CREATE TABLE engage$revenue_per_item_model_per_day AS (
    SELECT
      order_date date,
      outlet_type,
      persona,
      item_model,
      item_category,
      SUM(total_amount_with_tax) revenue,
      SUM(item_quantity) quantity_sold
    FROM engage$product_sold ps
    LEFT JOIN engage$outlets o
      ON o.outlet_code = ps.outlet_code
    LEFT JOIN engage$persona pers
      ON pers.customer_id = ps.customer_id
    GROUP BY 1, 2, 3, 4, 5
);

-- Check that engage$campaign_spending_per_model_per_day add up to campaign spending
SELECT
  assert(
      (SELECT
        bool_and(abs(a.spending - b.spending) < 0.01)
      FROM (SELECT campaign_id, SUM(spending) spending FROM engage$campaign_spending_per_model_per_day GROUP BY campaign_id) a
      LEFT JOIN (select campaign_id, item_weightage * spending spending FROM import.engage$campaigns) b
        ON a.campaign_id = b.campaign_id),
      'mismatch campaign_spending_per_model_per_day spending doesn''t add up to campaign spending'
  );





DROP TABLE IF EXISTS engage$campaign_performance;
-- CREATE TABLE engage$campaign_performance AS (
--   SELECT
--     cs.campaign_group,
--     SUM(item_revenue) item_revenue,
--     SUM(item_cost) item_cost,
--     SUM(item_revenue) / SUM(item_cost) performance
--   FROM (
--     SELECT
--       order_date,
--       item_model,
--       campaign_group,
--       SUM(cost) item_cost
--     FROM
--       engage$campaign_order_cost
--     WHERE cost_attribution = 'item'
--     GROUP BY 1, 2, 3
--   ) cs
--   LEFT JOIN (
--     SELECT
--       order_date,
--       item_model,
--       SUM(total_amount_with_tax) item_revenue
--     FROM engage$product_sold
--     GROUP BY order_date, item_model
--   ) ps
--   ON ps.order_date = cs.order_date AND ps.item_model = cs.item_model
--   GROUP BY campaign_group
-- );

DROP TABLE IF EXISTS engage$campaign_performance_per_model;
-- CREATE TABLE engage$campaign_performance_per_model AS (
--   SELECT
--     x.item_model,
--     i.item_category,
--     x.order_date,
--     item_revenue,
--     item_quantity,
--     item_cost
--   FROM
--     (
--     SELECT
--       cs.item_model,
--       cs.order_date,
--       SUM(item_revenue) item_revenue,
--       SUM(cs.item_cost) item_cost,
--       SUM(item_quantity) item_quantity
--     FROM (
--            SELECT
--              order_date,
--              item_model,
--              SUM(cost) item_cost
--            FROM
--              engage$campaign_order_cost
--            WHERE cost_attribution = 'item'
--            GROUP BY order_date, item_model
--          ) cs
--       LEFT JOIN (
--         SELECT
--           order_date,
--           item_model,
--           SUM(total_amount_with_tax) item_revenue,
--           COUNT(item_quantity) item_quantity
--         FROM engage$product_sold
--         GROUP BY order_date, item_model
--       ) ps
--         ON ps.order_date = cs.order_date AND ps.item_model = cs.item_model
--     GROUP BY cs.item_model, cs.order_date
--   ) x
--   LEFT JOIN engage$items_model_category i
--     ON x.item_model = i.item_model
-- );


DROP TABLE IF EXISTS engage$campaign_performance_per_product_no_roadshow;
-- CREATE TABLE engage$campaign_performance_per_product_no_roadshow AS (
--   SELECT
--     x.item_model,
--     i.item_category,
--     x.order_date,
--     item_revenue,
--     item_quantity,
--     item_cost
--   FROM
--     (
--     SELECT
--       cs.item_model,
--       cs.order_date,
--       SUM(item_revenue) item_revenue,
--       SUM(cs.item_cost) item_cost,
--       SUM(item_quantity) item_quantity
--     FROM (
--            SELECT
--              order_date,
--              item_model,
--              SUM(cost) item_cost
--            FROM
--              engage$campaign_order_cost
--            WHERE cost_attribution = 'item' AND channel != 'Roadshow'
--            GROUP BY order_date, item_model
--          ) cs
--       LEFT JOIN (
--         SELECT
--           order_date,
--           item_model,
--           SUM(total_amount_with_tax) item_revenue,
--           COUNT(item_quantity) item_quantity
--         FROM engage$product_sold ps
--         LEFT JOIN public.engage$outlets o
--           ON o.outlet_code = ps.outlet_code
--         WHERE o.outlet_type != 'FAIR'
--
--         GROUP BY order_date, item_model
--       ) ps
--         ON ps.order_date = cs.order_date AND ps.item_model = cs.item_model
--     GROUP BY cs.item_model, cs.order_date
--   ) x
--   LEFT JOIN engage$items_model_category i
--     ON x.item_model = i.item_model
-- );

DROP TABLE IF EXISTS engage$campaign_performance_per_product_roadshow_only;
-- CREATE TABLE engage$campaign_performance_per_product_roadshow_only AS (
--   SELECT
--     x.item_model,
--     i.item_category,
--     x.order_date,
--     item_revenue,
--     item_quantity,
--     item_cost
--   FROM
--     (
--     SELECT
--       cs.item_model,
--       cs.order_date,
--       SUM(item_revenue) item_revenue,
--       SUM(cs.item_cost) item_cost,
--       SUM(item_quantity) item_quantity
--     FROM (
--            SELECT
--              order_date,
--              item_model,
--              SUM(cost) item_cost
--            FROM
--              engage$campaign_order_cost
--            WHERE cost_attribution = 'item' AND channel = 'Roadshow'
--            GROUP BY order_date, item_model
--          ) cs
--       LEFT JOIN (
--         SELECT
--           order_date,
--           item_model,
--           SUM(total_amount_with_tax) item_revenue,
--           COUNT(item_quantity) item_quantity
--         FROM engage$product_sold ps
--         LEFT JOIN public.engage$outlets o
--           ON o.outlet_code = ps.outlet_code
--         WHERE o.outlet_type = 'FAIR'
--
--         GROUP BY order_date, item_model
--       ) ps
--         ON ps.order_date = cs.order_date AND ps.item_model = cs.item_model
--     GROUP BY cs.item_model, cs.order_date
--   ) x
--   LEFT JOIN engage$items_model_category i
--     ON x.item_model = i.item_model
-- );


-- Check that the spending part of performance is correct.
-- SELECT
--   assert(bool_and(abs(g.spending - item_cost) < 0.01), 'mismatch performance spending != campaign spending')
-- FROM (
--     SELECT campaign_group, SUM(item_weightage * spending) spending
--     FROM import.engage$campaigns
--     GROUP BY campaign_group
-- ) g
-- LEFT JOIN engage$campaign_performance p
--   ON p.campaign_group = g.campaign_group
-- WHERE g.spending > 0;


------------------------------------------------------------------------------------------------------------------------
-- CUSTOMER LEVEL                                                                                                     --
-- just aggregate.
------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS engage$customer_marketing_cost;
CREATE TABLE engage$customer_marketing_cost AS (
    SELECT
      c.customer_id,
      c.total_marketing_cost,
      c2.clv_3_5_years
    FROM (
      SELECT
        c.customer_id,
        SUM(c.cost) total_marketing_cost
      FROM engage$campaign_order_cost c
      GROUP BY c.customer_id
    ) c
  LEFT JOIN public.engage$customers c2
    ON c2.customer_id = c.customer_id
);


DROP TABLE IF EXISTS engage$persona_clv;
CREATE TABLE engage$persona_clv AS (
    SELECT
      x.persona,
      x.avg_clv_3_5_years,
      y.average_marketing_cost
    FROM (
      SELECT
        persona,
        AVG(clv_3_5_years) avg_clv_3_5_years
      FROM engage$customers c
      LEFT JOIN engage$persona p
        ON p.customer_id = c.customer_id
      WHERE member_cohort < 2015
      GROUP BY persona
    ) x
    LEFT JOIN (
      SELECT
        COALESCE(persona, 'I') persona, -- Set default person to 'I'
        AVG(total_marketing_cost) average_marketing_cost
      FROM engage$customer_marketing_cost mc
      LEFT JOIN engage$persona p
        ON mc.customer_id = p.customer_id
      GROUP BY 1
    ) y
    ON x.persona = y.persona
);


SELECT
  assert(
      abs((SELECT SUM(total_marketing_cost) FROM engage$customer_marketing_cost) - (SELECT SUM(spending) FROM engage$campaigns)) < 0.0001,
      'sum of customer marketing cost != total marketing expense ' ||
      (SELECT SUM(total_marketing_cost) FROM engage$customer_marketing_cost) || ' != ' ||
      (SELECT SUM(spending) FROM engage$campaigns)
  );


-- -- Get the amount of revenue generated by basket with campaign item links
-- -- in order to build the model for Revenue ~ $ per Channel
-- DROP TABLE IF EXISTS engage$campaign_revenue_per_channel;
-- CREATE TABLE engage$campaign_revenue_per_channel AS (
--     SELECT
--       c.campaign_group,
--       c.total_revenue,
--       c.nb_orders,
--       b.channel,
--       SUM(b.spending) total_channel_spending
--     FROM (
--     SELECT
--       c.campaign_group,
--       SUM(c.total) total_revenue,
--       COUNT(distinct(c.order_id)) nb_orders
--     FROM (
--       SELECT
--         c.campaign_group,
--         MAX(b.total) total,
--         c.order_id
--       FROM _campaign_customer_item_link c
--         LEFT JOIN engage$baskets b
--           ON c.order_id = b.order_id
--       GROUP BY c.campaign_group, c.order_id
--       ) c
--     GROUP BY campaign_group
--     ) c
--   LEFT JOIN engage$campaigns b
--     ON b.campaign_group = c.campaign_group
--   GROUP BY 1, 2, 3, 4
-- );
--
-- DROP TABLE IF EXISTS engage$campaign_revenue_per_channel_per_persona;
-- CREATE TABLE engage$campaign_revenue_per_channel_per_persona AS (
--     SELECT
--       c.campaign_group,
--       c.total_revenue,
--       c.nb_orders,
--       c.persona,
--       b.channel,
--       SUM(b.spending) total_channel_spending
--     FROM (
--     SELECT
--       c.campaign_group,
--       c.persona,
--       SUM(c.total) total_revenue,
--       COUNT(distinct(c.order_id)) nb_orders
--     FROM (
--       SELECT
--         c.campaign_group,
--         MAX(b.total) total,
--         c.order_id,
--         p.persona
--       FROM _campaign_customer_item_link c
--       LEFT JOIN engage$baskets b
--         ON c.order_id = b.order_id
--       LEFT JOIN engage$persona p
--         ON b.customer_id = p.customer_id
--       GROUP BY c.campaign_group, c.order_id, p.persona
--       ) c
--     GROUP BY campaign_group, persona
--     ) c
--   LEFT JOIN engage$campaigns b
--     ON b.campaign_group = c.campaign_group
--   GROUP BY 1, 2, 3, 4, 5
-- );
--
--
--
-- SELECT
--   assert(
--     abs(
--         (SELECT SUM(total_revenue)
--          FROM engage$campaign_revenue_per_channel_per_persona) -
--         (SELECT SUM(total_revenue)
--          FROM engage$campaign_revenue_per_channel)
--     ) < 0.01, 'Invalid revenue per channel / persona calculation'
--   ) "revenue per channel calculation";

SELECT assert(TRUE, 'all test passed') "all test passed!";


