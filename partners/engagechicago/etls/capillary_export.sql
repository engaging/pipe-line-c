-- 999-capillary-export.sql
-- Get data ready for capillary export.

BEGIN;

SELECT
  assert(
      (SELECT COUNT(DISTINCT(customer_id)) FROM public.engage$opportunity) =
      (SELECT COUNT(customer_id) FROM public.engage$opportunity), 'Duplicate customer_id in engage$opportunity')
;



DROP TABLE IF EXISTS engage$capillary_customers;
CREATE TABLE engage$capillary_customers AS (
    SELECT
      c.customer_id cCustomerID,
      c.clv_3_5_years clv_3_5_years,
      opp.opportunity::text,
      g.period_name,

      persona.persona Persona,

      c.frequency,
      c.recency,
      c.monetary_value,
      c.customer_address_type,
      ("nb_order_containing accessory" > 0)::int "has accessory",
      ("nb_order_containing air purifier" > 0)::int "has air purifier",
      ("nb_order_containing blood pressure monitor" > 0)::int "has blood pressure monitor",
      ("nb_order_containing body composition monitor" > 0)::int "has body composition monitor",
      ("nb_order_containing eye massager" > 0)::int "has eye massager",
      ("nb_order_containing family hygiene" > 0)::int "has family hygiene",
      ("nb_order_containing gel pad series" > 0)::int "has gel pad series",
      ("nb_order_containing handheld massager" > 0)::int "has handheld massager",
      ("nb_order_containing head massager" > 0)::int "has head massager",
      ("nb_order_containing humidifier" > 0)::int "has humidifier",
      ("nb_order_containing innovative fitness" > 0)::int "has innovative fitness",
      ("nb_order_containing leg massager" > 0)::int "has leg massager",
      ("nb_order_containing lower body massager" > 0)::int "has lower body massager",
      ("nb_order_containing massage chair" > 0)::int "has massage chair",
      ("nb_order_containing massage sofa" > 0)::int "has massage sofa",
      ("nb_order_containing pedometer" > 0)::int "has pedometer",
      ("nb_order_containing pulse massager" > 0)::int "has pulse massager",
      ("nb_order_containing sleep well series" > 0)::int "has sleep well series",
      ("nb_order_containing slim belts" > 0)::int "has slim belts",
      ("nb_order_containing thermometer" > 0)::int "has thermometer",
      ("nb_order_containing travel series" > 0)::int "has travel series",
      ("nb_order_containing ultrasonic cleaner" > 0)::int "has ultrasonic cleaner",
      ("nb_order_containing upper body massager" > 0)::int "has upper body massager",
      ("nb_order_containing vacuum cleaner" > 0)::int "has vacuum cleaner",
      ("nb_order_containing water purifier" > 0)::int "has water purifier",
      (c.customer_allow_email OR c.customer_allow_sms)::int "e_contactable",
      (nb_paid_baskets > 1)::int "has_more_than_one_visit"

    FROM engage$customers c
    LEFT JOIN import.engage$customers ic
      ON ic.customer_id = c.customer_id
    LEFT JOIN engage$persona persona
      ON persona.customer_id = c.customer_id
    LEFT JOIN engage$grid_base g
      ON CURRENT_DATE BETWEEN g.date_begin AND g.date_end

  LEFT JOIN public.engage$opportunity opp
      ON opp.customer_id = c.customer_id
);

COMMIT;
