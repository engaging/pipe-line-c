-- 021-customer-grid.sql
-- Build a grid for opportunity model calculation


DROP TABLE IF EXISTS _customer_prev_purchase;
CREATE TABLE _customer_prev_purchase
  DISTKEY(customer_id)
  SORTKEY(period_index)
  AS (
  SELECT
    g.period_index,
    g.date_begin,
    s.customer_id,
    s.item_category,
    MAX(s.order_date) order_date
  FROM engage$grid_base g
  LEFT JOIN engage$product_sold s
  ON s.order_date < g.date_begin

  WHERE s.item_category IN ('Upper Body Massager', 'Humidifier', 'Eye Massager', 'Air Purifier', 'Massage Sofa', 'Massage Chair', 'Handheld Massager', 'Head Massager', 'Leg Massager', 'Travel Series', 'Slim Belts', 'Water Purifier', 'Innovative Fitness', 'Lower Body Massager')
  GROUP BY date_begin, period_index, customer_id, item_category
);


DROP TABLE IF EXISTS customer_grid_y;
CREATE TABLE customer_grid_y AS (
    SELECT
      b.customer_id,
      grid.*,
      SUM(total) spent_in_period,

      BOOL_OR("has_category accessory") "nb_purchase accessory",
      BOOL_OR("has_category air purifier") "nb_purchase air purifier",
      BOOL_OR("has_category blood pressure monitor") "nb_purchase blood pressure monitor",
      BOOL_OR("has_category body composition monitor") "nb_purchase body composition monitor",
      BOOL_OR("has_category eye massager") "nb_purchase eye massager",
      BOOL_OR("has_category family hygiene") "nb_purchase family hygiene",
      BOOL_OR("has_category gel pad series") "nb_purchase gel pad series",
      BOOL_OR("has_category handheld massager") "nb_purchase handheld massager",
      BOOL_OR("has_category head massager") "nb_purchase head massager",
      BOOL_OR("has_category humidifier") "nb_purchase humidifier",
      BOOL_OR("has_category innovative fitness") "nb_purchase innovative fitness",
      BOOL_OR("has_category leg massager") "nb_purchase leg massager",
      BOOL_OR("has_category lower body massager") "nb_purchase lower body massager",
      BOOL_OR("has_category massage chair") "nb_purchase massage chair",
      BOOL_OR("has_category massage sofa") "nb_purchase massage sofa",
      BOOL_OR("has_category pedometer") "nb_purchase pedometer",
      BOOL_OR("has_category pulse massager") "nb_purchase pulse massager",
      BOOL_OR("has_category sleep well series") "nb_purchase sleep well series",
      BOOL_OR("has_category slim belts") "nb_purchase slim belts",
      BOOL_OR("has_category thermometer") "nb_purchase thermometer",
      BOOL_OR("has_category travel series") "nb_purchase travel series",
      BOOL_OR("has_category ultrasonic cleaner") "nb_purchase ultrasonic cleaner",
      BOOL_OR("has_category upper body massager") "nb_purchase upper body massager",
      BOOL_OR("has_category vacuum cleaner") "nb_purchase vacuum cleaner",
      BOOL_OR("has_category water purifier") "nb_purchase water purifier"

    FROM engage$baskets b
    LEFT JOIN engage$grid_base grid
    ON TRUE

    WHERE b.order_date BETWEEN grid.date_begin AND grid.date_end
    AND grid.date_begin < current_date
    GROUP BY b.customer_id, grid.period_name, grid.date_end, grid.date_begin, grid.period_index
); -- customer_grid_y


DROP TABLE IF EXISTS _grid_x;
CREATE TEMPORARY TABLE _grid_x AS (
    SELECT
      b.customer_id,
      grid.*,
      SUM(b.total) total_spent_before_period,
      AVG(days_since_last_purchase) average_days_between_sales_before_period,
      grid.date_begin - MAX(order_date) days_since_last_sales_from_beginning_of_period

    FROM engage$baskets b
    LEFT JOIN engage$grid_base grid
    ON TRUE

    WHERE b.order_date < grid.date_begin
      AND grid.date_begin < current_date
      AND b.total > 0
    GROUP BY b.customer_id, grid.period_name, grid.date_end, grid.date_begin, grid.period_index
); -- _grid_x


DROP TABLE IF EXISTS engage$customer_grid;
CREATE TABLE engage$customer_grid AS (
    -- z_: filtering, y_: shit to predict, the rest: shit to use as predictor
    SELECT
      x.customer_id z_customer_id,
      x.date_begin z_date_begin,
      x.date_end z_date_end,
      x.period_name z_period_name,
      x.period_index z_period_index,

      COALESCE(x.average_days_between_sales_before_period, -1) average_days_between_sales_before_period,
      COALESCE(x.days_since_last_sales_from_beginning_of_period, -1) days_since_last_sales_from_beginning_of_period,
      COALESCE(x.total_spent_before_period, 0) total_spent_before_period,

      COALESCE(y.spent_in_period, 0) y_spent_in_period,

      COALESCE(y."nb_purchase accessory"::int, 0) "y_has_purchase accessory",
      COALESCE(y."nb_purchase air purifier"::int, 0) "y_has_purchase air purifier",
      COALESCE(y."nb_purchase blood pressure monitor"::int, 0) "y_has_purchase blood pressure monitor",
      COALESCE(y."nb_purchase body composition monitor"::int, 0) "y_has_purchase body composition monitor",
      COALESCE(y."nb_purchase eye massager"::int, 0) "y_has_purchase eye massager",
      COALESCE(y."nb_purchase family hygiene"::int, 0) "y_has_purchase family hygiene",
      COALESCE(y."nb_purchase gel pad series"::int, 0) "y_has_purchase gel pad series",
      COALESCE(y."nb_purchase handheld massager"::int, 0) "y_has_purchase handheld massager",
      COALESCE(y."nb_purchase head massager"::int, 0) "y_has_purchase head massager",
      COALESCE(y."nb_purchase humidifier"::int, 0) "y_has_purchase humidifier",
      COALESCE(y."nb_purchase innovative fitness"::int, 0) "y_has_purchase innovative fitness",
      COALESCE(y."nb_purchase leg massager"::int, 0) "y_has_purchase leg massager",
      COALESCE(y."nb_purchase lower body massager"::int, 0) "y_has_purchase lower body massager",
      COALESCE(y."nb_purchase massage chair"::int, 0) "y_has_purchase massage chair",
      COALESCE(y."nb_purchase massage sofa"::int, 0) "y_has_purchase massage sofa",
      COALESCE(y."nb_purchase pedometer"::int, 0) "y_has_purchase pedometer",
      COALESCE(y."nb_purchase pulse massager"::int, 0) "y_has_purchase pulse massager",
      COALESCE(y."nb_purchase sleep well series"::int, 0) "y_has_purchase sleep well series",
      COALESCE(y."nb_purchase slim belts"::int, 0) "y_has_purchase slim belts",
      COALESCE(y."nb_purchase thermometer"::int, 0) "y_has_purchase thermometer",
      COALESCE(y."nb_purchase travel series"::int, 0) "y_has_purchase travel series",
      COALESCE(y."nb_purchase ultrasonic cleaner"::int, 0) "y_has_purchase ultrasonic cleaner",
      COALESCE(y."nb_purchase upper body massager"::int, 0) "y_has_purchase upper body massager",
      COALESCE(y."nb_purchase vacuum cleaner"::int, 0) "y_has_purchase vacuum cleaner",
      COALESCE(y."nb_purchase water purifier"::int, 0) "y_has_purchase water purifier",

      COALESCE(first_visit."nb_diff_items", 0) "first_visit_nb_diff_items",
      COALESCE(first_visit."total", 0) "first_visit_total",

      COALESCE(first_visit."has_category accessory"::int, 0) "first_visit_has_category accessory",
      COALESCE(first_visit."has_category air purifier"::int, 0) "first_visit_has_category air purifier",
      COALESCE(first_visit."has_category blood pressure monitor"::int, 0) "first_visit_has_category blood pressure monitor",
      COALESCE(first_visit."has_category body composition monitor"::int, 0) "first_visit_has_category body composition monitor",
      COALESCE(first_visit."has_category eye massager"::int, 0) "first_visit_has_category eye massager",
      COALESCE(first_visit."has_category family hygiene"::int, 0) "first_visit_has_category family hygiene",
      COALESCE(first_visit."has_category gel pad series"::int, 0) "first_visit_has_category gel pad series",
      COALESCE(first_visit."has_category handheld massager"::int, 0) "first_visit_has_category handheld massager",
      COALESCE(first_visit."has_category head massager"::int, 0) "first_visit_has_category head massager",
      COALESCE(first_visit."has_category humidifier"::int, 0) "first_visit_has_category humidifier",
      COALESCE(first_visit."has_category innovative fitness"::int, 0) "first_visit_has_category innovative fitness",
      COALESCE(first_visit."has_category leg massager"::int, 0) "first_visit_has_category leg massager",
      COALESCE(first_visit."has_category lower body massager"::int, 0) "first_visit_has_category lower body massager",
      COALESCE(first_visit."has_category massage chair"::int, 0) "first_visit_has_category massage chair",
      COALESCE(first_visit."has_category massage sofa"::int, 0) "first_visit_has_category massage sofa",
      COALESCE(first_visit."has_category pedometer"::int, 0) "first_visit_has_category pedometer",
      COALESCE(first_visit."has_category pulse massager"::int, 0) "first_visit_has_category pulse massager",
      COALESCE(first_visit."has_category sleep well series"::int, 0) "first_visit_has_category sleep well series",
      COALESCE(first_visit."has_category slim belts"::int, 0) "first_visit_has_category slim belts",
      COALESCE(first_visit."has_category thermometer"::int, 0) "first_visit_has_category thermometer",
      COALESCE(first_visit."has_category travel series"::int, 0) "first_visit_has_category travel series",
      COALESCE(first_visit."has_category ultrasonic cleaner"::int, 0) "first_visit_has_category ultrasonic cleaner",
      COALESCE(first_visit."has_category upper body massager"::int, 0) "first_visit_has_category upper body massager",
      COALESCE(first_visit."has_category vacuum cleaner"::int, 0) "first_visit_has_category vacuum cleaner",
      COALESCE(first_visit."has_category water purifier"::int, 0) "first_visit_has_category water purifier",

      COALESCE(x.date_begin - first_visit.order_date, -1) time_since_first_visit,

      COALESCE(second_visit."nb_diff_items", 0) "second_visit_nb_diff_items",
      COALESCE(second_visit."total", -1) "second_visit_total",

      COALESCE(second_visit."has_category accessory"::int, 0) "second_visit_has_category accessory",
      COALESCE(second_visit."has_category air purifier"::int, 0) "second_visit_has_category air purifier",
      COALESCE(second_visit."has_category blood pressure monitor"::int, 0) "second_visit_has_category blood pressure monitor",
      COALESCE(second_visit."has_category body composition monitor"::int, 0) "second_visit_has_category body composition monitor",
      COALESCE(second_visit."has_category eye massager"::int, 0) "second_visit_has_category eye massager",
      COALESCE(second_visit."has_category family hygiene"::int, 0) "second_visit_has_category family hygiene",
      COALESCE(second_visit."has_category gel pad series"::int, 0) "second_visit_has_category gel pad series",
      COALESCE(second_visit."has_category handheld massager"::int, 0) "second_visit_has_category handheld massager",
      COALESCE(second_visit."has_category head massager"::int, 0) "second_visit_has_category head massager",
      COALESCE(second_visit."has_category humidifier"::int, 0) "second_visit_has_category humidifier",
      COALESCE(second_visit."has_category innovative fitness"::int, 0) "second_visit_has_category innovative fitness",
      COALESCE(second_visit."has_category leg massager"::int, 0) "second_visit_has_category leg massager",
      COALESCE(second_visit."has_category lower body massager"::int, 0) "second_visit_has_category lower body massager",
      COALESCE(second_visit."has_category massage chair"::int, 0) "second_visit_has_category massage chair",
      COALESCE(second_visit."has_category massage sofa"::int, 0) "second_visit_has_category massage sofa",
      COALESCE(second_visit."has_category pedometer"::int, 0) "second_visit_has_category pedometer",
      COALESCE(second_visit."has_category pulse massager"::int, 0) "second_visit_has_category pulse massager",
      COALESCE(second_visit."has_category sleep well series"::int, 0) "second_visit_has_category sleep well series",
      COALESCE(second_visit."has_category slim belts"::int, 0) "second_visit_has_category slim belts",
      COALESCE(second_visit."has_category thermometer"::int, 0) "second_visit_has_category thermometer",
      COALESCE(second_visit."has_category travel series"::int, 0) "second_visit_has_category travel series",
      COALESCE(second_visit."has_category ultrasonic cleaner"::int, 0) "second_visit_has_category ultrasonic cleaner",
      COALESCE(second_visit."has_category upper body massager"::int, 0) "second_visit_has_category upper body massager",
      COALESCE(second_visit."has_category vacuum cleaner"::int, 0) "second_visit_has_category vacuum cleaner",
      COALESCE(second_visit."has_category water purifier"::int, 0) "second_visit_has_category water purifier",

      COALESCE(x.date_begin - second_visit.order_date, -1) time_since_second_visit,

      COALESCE(x.date_begin - p_chair.order_date, -1) time_since_last_purchase_chair,
      COALESCE(x.date_begin - p_sofa.order_date, -1)  time_since_last_purchase_sofa,
      COALESCE(x.date_begin - p_cat1.order_date, -1)  time_since_last_purchase_upper_body,
      COALESCE(x.date_begin - p_cat2.order_date, -1)  time_since_last_purchase_humidifier,
      COALESCE(x.date_begin - p_cat3.order_date, -1)  time_since_last_purchase_eye_massager,
      COALESCE(x.date_begin - p_cat4.order_date, -1)  time_since_last_purchase_air_purifier,
      COALESCE(x.date_begin - p_cat5.order_date, -1)  time_since_last_purchase_handheld_massager,
      COALESCE(x.date_begin - p_cat6.order_date, -1)  time_since_last_purchase_head_massager,
      COALESCE(x.date_begin - p_cat7.order_date, -1)  time_since_last_purchase_leg_massager,
      COALESCE(x.date_begin - p_cat8.order_date, -1)  time_since_last_purchase_travel_series,
      COALESCE(x.date_begin - p_cat9.order_date, -1)  time_since_last_purchase_lower_body,

      COALESCE(CASE WHEN c.customer_dob IS NOT NULL
           THEN (x.date_begin - c.customer_dob)::INT / 365
           ELSE NULL
      END, 0) customer_age_at_beginning_period,
      c.customer_gender cat_gender,
      c.customer_address_district cat_address_district,
      c.customer_address_type cat_address_type

    FROM _grid_x x
    LEFT JOIN customer_grid_y y
    ON y.date_begin = x.date_begin AND y.customer_id = x.customer_id

    LEFT JOIN engage$baskets first_visit
    ON first_visit.customer_id = x.customer_id AND first_visit.nth_basket = 1 AND first_visit.order_date < x.date_begin

    LEFT JOIN engage$baskets second_visit
    ON second_visit.customer_id = x.customer_id AND second_visit.nth_basket = 2 AND second_visit.order_date < x.date_begin

    LEFT JOIN engage$customers c
    ON c.customer_id = x.customer_id

    LEFT JOIN _customer_prev_purchase p_chair
    ON p_chair.customer_id = c.customer_id
    AND p_chair.item_category = 'Massage Chair'
    AND p_chair.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_sofa
    ON p_sofa.customer_id = c.customer_id
    AND p_sofa.item_category = 'Massage Sofa'
    AND p_sofa.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat1
    ON p_cat1.customer_id = c.customer_id
    AND p_cat1.item_category = 'Upper Body Massager'
    AND p_cat1.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat2
    ON p_cat2.customer_id = c.customer_id
    AND p_cat2.item_category = 'Humidifier'
    AND p_cat2.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat3
    ON p_cat3.customer_id = c.customer_id
    AND p_cat3.item_category = 'Eye Massager'
    AND p_cat3.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat4
    ON p_cat4.customer_id = c.customer_id
    AND p_cat4.item_category = 'Air Purifier'
    AND p_cat4.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat5
    ON p_cat5.customer_id = c.customer_id
    AND p_cat5.item_category = 'Handheld Massager'
    AND p_cat5.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat6
    ON p_cat6.customer_id = c.customer_id
    AND p_cat6.item_category = 'Head Massager'
    AND p_cat6.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat7
    ON p_cat7.customer_id = c.customer_id
    AND p_cat7.item_category = 'Leg Massager'
    AND p_cat7.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat8
    ON p_cat8.customer_id = c.customer_id
    AND p_cat8.item_category = 'Travel Series'
    AND p_cat8.period_index = x.period_index

    LEFT JOIN _customer_prev_purchase p_cat9
    ON p_cat9.customer_id = c.customer_id
    AND p_cat9.item_category = 'Lower Body Massager'
    AND p_cat9.period_index = x.period_index

    WHERE x.date_begin < current_date
); -- engage$customer_grid

