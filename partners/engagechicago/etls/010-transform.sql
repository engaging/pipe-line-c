-- 010-transform.sql
-- Basic data import; customer / basket views

------------------------------------------------------------------------------------------------------------------------
-- BASKETS
------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS _engage$transactions;
CREATE TEMPORARY TABLE _engage$transactions
  DISTKEY(order_id) SORTKEY(order_id, order_date)
  AS (
  SELECT
    "completed",
    "created_datetime"::TIMESTAMP,
    "customer_id",
    "discount_pc",
    "first_order_date"::DATE,
    CASE WHEN item_category IS NULL AND item_type = 'TSS'
    THEN 'Maintenance'
    ELSE item_category END item_category,
    "item_description",
    "item_model_description",
    "item_discount",
    "item_group",
    "item_model",
    "item_no",
    "item_price",
    "item_quantity",
    "item_type",
    "line_number"::INT,
    "nb_redemption_used",
    "nb_voucher_used",
    "order_date"::DATE,
    "order_number" "order_id",
    "outlet_address",
    "outlet_address_district",
    "outlet_address_type",
    "outlet_code",
    "outlet_type",
    "outlet_zip",
    "outlet_code" in ('AS-ET-AT2', 'AS-ET-AT3') "airport_outlet",
    "promo_description",
    "promo_end_date"::DATE,
    "promo_id",
    "promo_line_no"::INT,
    "promo_start_date"::DATE,
    "salesperson",
    "total_amount",
    "total_amount_with_tax",
    "total_discount_from_voucher",

    "is_category accessory",
    "is_category air purifier",
    "is_category blood pressure monitor",
    "is_category body composition monitor",
    "is_category eye massager",
    "is_category family hygiene",
    "is_category gel pad series",
    "is_category handheld massager",
    "is_category head massager",
    "is_category humidifier",
    "is_category innovative fitness",
    "is_category leg massager",
    "is_category lower body massager",
    "is_category massage chair",
    "is_category massage sofa",
    "is_category pedometer",
    "is_category pulse massager",
    "is_category sleep well series",
    "is_category slim belts",
    "is_category thermometer",
    "is_category travel series",
    "is_category uitrasonic cleaner" "is_category ultrasonic cleaner",
    "is_category upper body massager",
    "is_category vacuum cleaner",
    "is_category water purifier",

--     "is_attribute habit building",
--     "is_attribute special occasion",
--     "is_attribute discount bargain hunter",
--     "is_attribute luxury",
--     "is_attribute home",
--     "is_attribute travel",
--     "is_attribute athlete",
--     "is_attribute beauty",
--     "is_attribute pain relief",
--     "is_attribute diet",
--     "is_attribute health",
--     "is_attribute light relaxation",
--     "is_attribute children oriented",

    CASE WHEN "is_category accessory" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category accessory",
    CASE WHEN "is_category air purifier" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category air purifier",
    CASE WHEN "is_category blood pressure monitor" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category blood pressure monitor",
    CASE WHEN "is_category body composition monitor" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category body composition monitor",
    CASE WHEN "is_category eye massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category eye massager",
    CASE WHEN "is_category family hygiene" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category family hygiene",
    CASE WHEN "is_category gel pad series" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category gel pad series",
    CASE WHEN "is_category handheld massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category handheld massager",
    CASE WHEN "is_category head massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category head massager",
    CASE WHEN "is_category humidifier" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category humidifier",
    CASE WHEN "is_category innovative fitness" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category innovative fitness",
    CASE WHEN "is_category leg massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category leg massager",
    CASE WHEN "is_category lower body massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category lower body massager",
    CASE WHEN "is_category massage chair" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category massage chair",
    CASE WHEN "is_category massage sofa" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category massage sofa",
    CASE WHEN "is_category pedometer" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category pedometer",
    CASE WHEN "is_category pulse massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category pulse massager",
    CASE WHEN "is_category sleep well series" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category sleep well series",
    CASE WHEN "is_category slim belts" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category slim belts",
    CASE WHEN "is_category thermometer" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category thermometer",
    CASE WHEN "is_category travel series" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category travel series",
    CASE WHEN "is_category uitrasonic cleaner" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category ultrasonic cleaner",
    CASE WHEN "is_category upper body massager" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category upper body massager",
    CASE WHEN "is_category vacuum cleaner" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category vacuum cleaner",
    CASE WHEN "is_category water purifier" THEN "total_amount_with_tax" ELSE 0 END "total_amount_category water purifier",

--     CASE WHEN "is_attribute habit building" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute habit building",
--     CASE WHEN "is_attribute special occasion" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute special occasion",
--     CASE WHEN "is_attribute discount bargain hunter" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute discount bargain hunter",
--     CASE WHEN "is_attribute luxury" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute luxury",
--     CASE WHEN "is_attribute home" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute home",
--     CASE WHEN "is_attribute travel" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute travel",
--     CASE WHEN "is_attribute athlete" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute athlete",
--     CASE WHEN "is_attribute beauty" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute beauty",
--     CASE WHEN "is_attribute pain relief" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute pain relief",
--     CASE WHEN "is_attribute diet" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute diet",
--     CASE WHEN "is_attribute health" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute health",
--     CASE WHEN "is_attribute light relaxation" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute light relaxation",
--     CASE WHEN "is_attribute children oriented" THEN "total_amount_with_tax" ELSE 0 END "total_amount_attribute children oriented",

    "is_freebie",
    "is_gwp",
    "is_paid_item",
    "is_paid_product",
    "is_returning_customer",
    "is_technical_service",
    "is_trade_in",
    "is_warranty",
    "is_birthday_promotion"
  FROM import.engage$sales
); -- _engage$transactions


DROP TABLE IF EXISTS _engage$baskets;
CREATE TEMPORARY TABLE _engage$baskets AS (
  WITH mothersday AS (SELECT
    date '2010-05-09' d2010,
    date '2011-05-08' d2011,
    date '2012-05-13' d2012,
    date '2013-05-12' d2013,
    date '2014-05-11' d2014,
    date '2015-05-10' d2015,
    date '2016-05-08' d2016,
    date '2017-05-14' d2017,
    date '2018-05-13' d2018
  )
  SELECT
    ts.order_id order_id,
    ts.customer_id customer_id,
    ts.salesperson,
    COUNT(ts.item_no) nb_diff_items,
    SUM(ts.total_amount_with_tax) total,
    ts.order_date order_date,
    ts.created_datetime created_datetime,
    BOOL_OR(ts.is_returning_customer) is_returning_customer,
    MAX(ts.promo_line_no::INT) nb_promo_used,
    SUM(ts.is_trade_in::INT) nb_trade_in,
    SUM(ts.nb_voucher_used) nb_vouchers_used,
    SUM(ts.nb_redemption_used) nb_redemption_used,
    SUM(ts.total_discount_from_voucher) total_discount_from_voucher,
    SUM(ts.is_freebie::INT) nb_freebies,
    SUM(ts.is_gwp::INT) nb_gift_with_purchases,
    SUM(ts.is_paid_item::INT) nb_paid_items,
    SUM(ts.is_paid_product::INT) nb_paid_products,
    BOOL_OR(ts.is_technical_service) has_technical_service,
    BOOL_OR(ts.is_warranty) has_warranty,
    ts.outlet_code outlet_code,
    ts.outlet_type outlet_type,
    ts.airport_outlet airport_outlet,

    BOOL_OR(ts."is_category accessory") "has_category accessory",
    BOOL_OR(ts."is_category air purifier") "has_category air purifier",
    BOOL_OR(ts."is_category blood pressure monitor") "has_category blood pressure monitor",
    BOOL_OR(ts."is_category body composition monitor") "has_category body composition monitor",
    BOOL_OR(ts."is_category eye massager") "has_category eye massager",
    BOOL_OR(ts."is_category family hygiene") "has_category family hygiene",
    BOOL_OR(ts."is_category gel pad series") "has_category gel pad series",
    BOOL_OR(ts."is_category handheld massager") "has_category handheld massager",
    BOOL_OR(ts."is_category head massager") "has_category head massager",
    BOOL_OR(ts."is_category humidifier") "has_category humidifier",
    BOOL_OR(ts."is_category innovative fitness") "has_category innovative fitness",
    BOOL_OR(ts."is_category leg massager") "has_category leg massager",
    BOOL_OR(ts."is_category lower body massager") "has_category lower body massager",
    BOOL_OR(ts."is_category massage chair") "has_category massage chair",
    BOOL_OR(ts."is_category massage sofa") "has_category massage sofa",
    BOOL_OR(ts."is_category pedometer") "has_category pedometer",
    BOOL_OR(ts."is_category pulse massager") "has_category pulse massager",
    BOOL_OR(ts."is_category sleep well series") "has_category sleep well series",
    BOOL_OR(ts."is_category slim belts") "has_category slim belts",
    BOOL_OR(ts."is_category thermometer") "has_category thermometer",
    BOOL_OR(ts."is_category travel series") "has_category travel series",
    BOOL_OR(ts."is_category ultrasonic cleaner") "has_category ultrasonic cleaner",
    BOOL_OR(ts."is_category upper body massager") "has_category upper body massager",
    BOOL_OR(ts."is_category vacuum cleaner") "has_category vacuum cleaner",
    BOOL_OR(ts."is_category water purifier") "has_category water purifier",
    BOOL_OR(ts."is_birthday_promotion") "has_birthday_promotion",

--     BOOL_OR(ts."is_attribute habit building") "has_attribute habit building",
--     BOOL_OR(ts."is_attribute special occasion") "has_attribute special occasion",
--     BOOL_OR(ts."is_attribute discount bargain hunter") "has_attribute discount bargain hunter",
--     BOOL_OR(ts."is_attribute luxury") "has_attribute luxury",
--     BOOL_OR(ts."is_attribute home") "has_attribute home",
--     BOOL_OR(ts."is_attribute travel") "has_attribute travel",
--     BOOL_OR(ts."is_attribute athlete") "has_attribute athlete",
--     BOOL_OR(ts."is_attribute beauty") "has_attribute beauty",
--     BOOL_OR(ts."is_attribute pain relief") "has_attribute pain relief",
--     BOOL_OR(ts."is_attribute diet") "has_attribute diet",
--     BOOL_OR(ts."is_attribute health") "has_attribute health",
--     BOOL_OR(ts."is_attribute light relaxation") "has_attribute light relaxation",
--     BOOL_OR(ts."is_attribute children oriented") "has_attribute children oriented",

    SUM("total_amount_category accessory") "total_amount_category accessory",
    SUM("total_amount_category air purifier") "total_amount_category air purifier",
    SUM("total_amount_category blood pressure monitor") "total_amount_category blood pressure monitor",
    SUM("total_amount_category body composition monitor") "total_amount_category body composition monitor",
    SUM("total_amount_category eye massager") "total_amount_category eye massager",
    SUM("total_amount_category family hygiene") "total_amount_category family hygiene",
    SUM("total_amount_category gel pad series") "total_amount_category gel pad series",
    SUM("total_amount_category handheld massager") "total_amount_category handheld massager",
    SUM("total_amount_category head massager") "total_amount_category head massager",
    SUM("total_amount_category humidifier") "total_amount_category humidifier",
    SUM("total_amount_category innovative fitness") "total_amount_category innovative fitness",
    SUM("total_amount_category leg massager") "total_amount_category leg massager",
    SUM("total_amount_category lower body massager") "total_amount_category lower body massager",
    SUM("total_amount_category massage chair") "total_amount_category massage chair",
    SUM("total_amount_category massage sofa") "total_amount_category massage sofa",
    SUM("total_amount_category pedometer") "total_amount_category pedometer",
    SUM("total_amount_category pulse massager") "total_amount_category pulse massager",
    SUM("total_amount_category sleep well series") "total_amount_category sleep well series",
    SUM("total_amount_category slim belts") "total_amount_category slim belts",
    SUM("total_amount_category thermometer") "total_amount_category thermometer",
    SUM("total_amount_category travel series") "total_amount_category travel series",
    SUM("total_amount_category ultrasonic cleaner") "total_amount_category ultrasonic cleaner",
    SUM("total_amount_category upper body massager") "total_amount_category upper body massager",
    SUM("total_amount_category vacuum cleaner") "total_amount_category vacuum cleaner",
    SUM("total_amount_category water purifier") "total_amount_category water purifier",

--     SUM("total_amount_attribute habit building") "total_amount_attribute habit building",
--     SUM("total_amount_attribute special occasion") "total_amount_attribute special occasion",
--     SUM("total_amount_attribute discount bargain hunter") "total_amount_attribute discount bargain hunter",
--     SUM("total_amount_attribute luxury") "total_amount_attribute luxury",
--     SUM("total_amount_attribute home") "total_amount_attribute home",
--     SUM("total_amount_attribute travel") "total_amount_attribute travel",
--     SUM("total_amount_attribute athlete") "total_amount_attribute athlete",
--     SUM("total_amount_attribute beauty") "total_amount_attribute beauty",
--     SUM("total_amount_attribute pain relief") "total_amount_attribute pain relief",
--     SUM("total_amount_attribute diet") "total_amount_attribute diet",
--     SUM("total_amount_attribute health") "total_amount_attribute health",
--     SUM("total_amount_attribute light relaxation") "total_amount_attribute light relaxation",
--     SUM("total_amount_attribute children oriented") "total_amount_attribute children oriented",

    (((ts.order_date) > ((SELECT d2010 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2010 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2011 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2011 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2012 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2012 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2013 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2013 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2014 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2014 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2015 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2015 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2016 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2016 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2017 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2017 FROM mothersday) + INTERVAL '5 days'))) OR
    (((ts.order_date) > ((SELECT d2018 FROM mothersday) - INTERVAL '15 days')) AND ((ts.order_date) < ((SELECT d2018 FROM mothersday) + INTERVAL '5 days'))) is_motherday_sales

  FROM _engage$transactions ts

  GROUP BY ts.order_id, ts.customer_id, ts.order_date, ts.created_datetime, ts.outlet_code, ts.outlet_type, ts.airport_outlet, ts.salesperson
); -- _engage$baskets

DROP TABLE IF EXISTS _basket_extras1;
CREATE TEMPORARY TABLE _basket_extras1 AS (
  SELECT
    b.order_id,
    b.nb_diff_items,
    b.total,
    o.main_item_description,
    o.main_item_model_description,
    o.main_item_category,
    o.main_item_amount_paid,
    o.main_item_quantity,
    o.main_item_promo_id,
    o.main_item_promo_description,
    o.main_item_model,
    o.main_item_group
  FROM _engage$baskets b
  LEFT JOIN (
    SELECT
      t.order_id,
      t.item_description main_item_description,
      t.item_model_description main_item_model_description,
      t.item_model main_item_model,
      t.total_amount_with_tax main_item_amount_paid,
      t.item_quantity main_item_quantity,
      t.item_category main_item_category,
      t.promo_description main_item_promo_description,
      t.promo_id main_item_promo_id,
      t.item_group main_item_group,
      ROW_NUMBER() OVER (PARTITION BY order_id ORDER BY total_amount_with_tax DESC, item_price DESC) pos
    FROM _engage$transactions t
    WHERE t.item_quantity > 0
  ) o
  ON o.order_id = b.order_id AND o.pos = 1
);

DROP TABLE IF EXISTS _basket_extras;
CREATE TEMPORARY TABLE _basket_extras AS (
  SELECT
    b.order_id,
    b.order_date - lag(b.order_date) OVER (PARTITION BY b.customer_id ORDER BY b.order_date ASC, b.created_datetime ASC) days_since_last_basket,
    lag(b.order_date) OVER (PARTITION BY b.customer_id ORDER BY b.order_date DESC, b.created_datetime DESC) - b.order_date  days_to_next_basket,

    ROW_NUMBER() OVER (PARTITION BY b.customer_id ORDER BY b.order_date, b.created_datetime ASC) nth_basket,

    e.main_item_description,
    e.main_item_model_description,
    e.main_item_category,
    e.main_item_amount_paid,
    e.main_item_quantity,
    e.main_item_promo_id,
    e.main_item_promo_description,
    e.main_item_model,
    e.main_item_group,

    c.first_order_date::date customer_first_order_date,
    EXTRACT('days' FROM (b.order_date - c.first_order_date::TIMESTAMP)) days_from_customer_first_order,
    EXTRACT('days' FROM (b.order_date - c.first_order_date::TIMESTAMP))::INT / 365 years_from_customer_first_order

  FROM _engage$baskets b
  LEFT JOIN _basket_extras1 e
  ON b.order_id = e.order_id

  LEFT JOIN import.engage$customers c
  ON c.customer_id = b.customer_id

  ORDER BY b.customer_id, b.order_date DESC
); -- _basket_extras

DROP TABLE IF EXISTS _basket_extras_paid;
CREATE TEMPORARY TABLE _basket_extras_paid AS (
  SELECT
    b.order_id,
    b.order_date - lag(b.order_date) OVER (PARTITION BY b.customer_id ORDER BY b.order_date ASC, b.created_datetime ASC) days_since_last_purchase,
    lag(b.order_date) OVER (PARTITION BY b.customer_id ORDER BY b.order_date DESC, b.created_datetime DESC) - b.order_date  days_to_next_purchase

  FROM _engage$baskets b
  LEFT JOIN _basket_extras1 e
  ON b.order_id = e.order_id

  LEFT JOIN import.engage$customers c
  ON c.customer_id = b.customer_id

  WHERE main_item_amount_paid > 0

  ORDER BY b.customer_id, b.order_date DESC
); -- _basket_extras_paid


DROP TABLE IF EXISTS public.engage$baskets;
CREATE TABLE public.engage$baskets AS (
    SELECT
      b.*,
      d.days_since_last_basket,
      d.days_to_next_basket,
      d.nth_basket,

      nth_paid_basket.pos nth_paid_basket,
      nth_paid_basket_from_last.pos nth_paid_basket_from_last,

      d.main_item_description,
      d.main_item_model_description,
      d.main_item_category,
      d.main_item_amount_paid,
      d.main_item_quantity,
      d.main_item_promo_id,
      d.main_item_promo_description,
      d.main_item_model,
      d.main_item_group,

      d.customer_first_order_date,
      d.days_from_customer_first_order days_from_customer_first_order,
      d.years_from_customer_first_order years_from_customer_first_order,

      d2.days_since_last_purchase,
      d2.days_to_next_purchase,

      -- 10 days before or 5 days after one's birthday
      CASE WHEN c.customer_month_of_birth IS NOT NULL and c.customer_day_of_birth IS NOT NULL
      THEN
        to_date(EXTRACT(YEAR FROM b.order_date)::text || '-' || LPAD(c.customer_month_of_birth::text, 2, '0') || '-' || LPAD(c.customer_day_of_birth::text, 2, '0'), 'YYYY-MM-DD') - b.order_date <= 15 AND
        to_date(EXTRACT(YEAR FROM b.order_date) || '-' || LPAD(c.customer_month_of_birth::text, 2, '0') || '-' || LPAD(c.customer_day_of_birth::text, 2, '0'), 'YYYY-MM-DD') - b.order_date > -10
        ELSE NULL
      END close_to_birthday,

      CASE WHEN c.customer_month_of_birth IS NOT NULL and c.customer_day_of_birth IS NOT NULL
      THEN
        to_date(EXTRACT(YEAR FROM b.order_date)::text || '-' || LPAD(c.customer_month_of_birth::text, 2, '0') || '-' || LPAD(c.customer_day_of_birth::text, 2, '0'), 'YYYY-MM-DD')
        ELSE NULL
      END customer_birthday,

      CASE WHEN c.customer_month_of_birth IS NOT NULL and c.customer_day_of_birth IS NOT NULL
      THEN
        to_date(EXTRACT(YEAR FROM b.order_date)::text || '-' || LPAD(c.customer_month_of_birth::text, 2, '0') || '-' || LPAD(c.customer_day_of_birth::text, 2, '0'), 'YYYY-MM-DD') - b.order_date
        ELSE NULL
      END time_to_birthday

    FROM _engage$baskets b

    LEFT JOIN _basket_extras d
    ON d.order_id = b.order_id

    LEFT JOIN _basket_extras_paid d2
    ON d2.order_id = b.order_id

    LEFT JOIN import.engage$customers c
    ON c.customer_id = b.customer_id

    LEFT JOIN (
      SELECT
        order_id,
        ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY order_date ASC, created_datetime ASC) pos
      FROM _engage$baskets
      WHERE total > 0
    ) nth_paid_basket
    ON nth_paid_basket.order_id = b.order_id

    LEFT JOIN (
      SELECT
        order_id,
        ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY order_date DESC, created_datetime DESC) pos
      FROM _engage$baskets
      WHERE total > 0
    ) nth_paid_basket_from_last
    ON nth_paid_basket_from_last.order_id = b.order_id

);


------------------------------------------------------------------------------------------------------------------------
-- Customers
------------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS _customership_calendar;
CREATE TEMPORARY TABLE _customership_calendar
  SORTKEY(customer_id)
  AS (
      SELECT
        b.customer_id,
        EXTRACT('year' FROM b.order_date) calendar_year,
        SUM(b.total) year_total,
        COUNT(b.total) year_nb_basket
      FROM public.engage$baskets b
      GROUP BY b.customer_id, EXTRACT('year' FROM b.order_date)
);

DROP TABLE IF EXISTS _customership_yearly;
CREATE TEMPORARY TABLE _customership_yearly
  SORTKEY(customer_id)
  AS (
      SELECT
        b.customer_id,
        b.years_from_customer_first_order customership_year,
        SUM(b.total) year_total,
        COUNT(b.total) year_nb_basket
      FROM public.engage$baskets b
      GROUP BY b.customer_id, b.years_from_customer_first_order
);


DROP TABLE IF EXISTS _customer_value;
CREATE TEMPORARY TABLE _customer_value AS (
    SELECT
      c.customer_id,
      big_stuff.total_amount_with_tax big_stuff_value,
      small_stuff.total_amount_with_tax small_stuff_value

    FROM import.engage$customers c
    LEFT JOIN (
        SELECT
          customer_id,
          SUM(total_amount_with_tax) total_amount_with_tax
        FROM _engage$transactions
        WHERE item_quantity > 0
        AND item_category in ('Massage Sofa','Massage Chair')
        AND order_date < first_order_date + INTERVAL '730 days'
        GROUP BY customer_id
    ) big_stuff
    ON big_stuff.customer_id = c.customer_id

    LEFT JOIN (
        SELECT
          customer_id,
          SUM(total_amount_with_tax) total_amount_with_tax
        FROM _engage$transactions
        WHERE item_quantity > 0
        AND item_category NOT IN ('Massage Sofa','Massage Chair')
        AND order_date < first_order_date + INTERVAL '730 days'
        GROUP BY customer_id
    ) small_stuff
    ON small_stuff.customer_id = c.customer_id
);
--
-- DROP TABLE IF EXISTS nth_paid_basket;
-- CREATE TEMPORARY TABLE nth_paid_basket AS (
--   SELECT
--     customer_id,
--     main_item_model, main_item_promo_description, main_item_promo_id, main_item_quantity,
--     main_item_amount_paid, main_item_category, main_item_description, salesperson, nb_diff_items,
--     total, order_date, nb_promo_used, nb_trade_in, nb_vouchers_used, nb_redemption_used,
--     total_discount_from_voucher, nb_freebies, nb_gift_with_purchases, nb_paid_items,
--     nb_paid_products, has_technical_service, has_warranty, outlet_code, outlet_type,
--     ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY order_date ASC) nth_basket,
--     ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY order_date DESC) nth_from_last
--   FROM engage$baskets
--   WHERE main_item_amount_paid > 0
-- );

DROP TABLE IF EXISTS public.engage$customers;
CREATE TABLE public.engage$customers
  SORTKEY(customer_id)
  AS (
    SELECT
      c.customer_id,
      c.customer_address_district,
      c.customer_address_type,
      c.customer_age,
      c.customer_allow_email,
      c.customer_allow_mail,
      c.customer_allow_sms,
      c.customer_created_date,
      c.customer_day_of_birth,
      c.customer_gender,
      c.customer_has_contact,
      c.customer_has_email,
      c.customer_month_of_birth,
      c.customer_year_of_birth,
      c.customer_zip,
      c.first_order_date,

      CASE WHEN c.customer_year_of_birth IS NOT NULL AND c.customer_month_of_birth IS NOT NULL AND c.customer_day_of_birth IS NOT NULL
      THEN
        to_date(c.customer_year_of_birth::text || '-' || LPAD(c.customer_month_of_birth, 2, '0') || '-' || LPAD(c.customer_day_of_birth::text, 2, '0'), 'YYYY-MM-DD')
      ELSE NULL
      END customer_dob,

      b_paid.average_days_between_purchases,
      b_paid.average_purchase_total,
      b_paid.total_value,
      b_paid.nb_paid_baskets,
      b_paid.max_paid_basket_total,

      b_paid."nb_motherday_purchase",
      b_paid."nb_purchase_close_to_birthday",

      COALESCE(b_all.nb_baskets, 0) nb_baskets,
      COALESCE(b_all."nb_order_containing accessory", 0) "nb_order_containing accessory",
      COALESCE(b_all."nb_order_containing air purifier", 0) "nb_order_containing air purifier",
      COALESCE(b_all."nb_order_containing blood pressure monitor", 0) "nb_order_containing blood pressure monitor",
      COALESCE(b_all."nb_order_containing body composition monitor", 0) "nb_order_containing body composition monitor",
      COALESCE(b_all."nb_order_containing eye massager", 0) "nb_order_containing eye massager",
      COALESCE(b_all."nb_order_containing family hygiene", 0) "nb_order_containing family hygiene",
      COALESCE(b_all."nb_order_containing gel pad series", 0) "nb_order_containing gel pad series",
      COALESCE(b_all."nb_order_containing handheld massager", 0) "nb_order_containing handheld massager",
      COALESCE(b_all."nb_order_containing head massager", 0) "nb_order_containing head massager",
      COALESCE(b_all."nb_order_containing humidifier", 0) "nb_order_containing humidifier",
      COALESCE(b_all."nb_order_containing innovative fitness", 0) "nb_order_containing innovative fitness",
      COALESCE(b_all."nb_order_containing leg massager", 0) "nb_order_containing leg massager",
      COALESCE(b_all."nb_order_containing lower body massager", 0) "nb_order_containing lower body massager",
      COALESCE(b_all."nb_order_containing massage chair", 0) "nb_order_containing massage chair",
      COALESCE(b_all."nb_order_containing massage sofa", 0) "nb_order_containing massage sofa",
      COALESCE(b_all."nb_order_containing pedometer", 0) "nb_order_containing pedometer",
      COALESCE(b_all."nb_order_containing pulse massager", 0) "nb_order_containing pulse massager",
      COALESCE(b_all."nb_order_containing sleep well series", 0) "nb_order_containing sleep well series",
      COALESCE(b_all."nb_order_containing slim belts", 0) "nb_order_containing slim belts",
      COALESCE(b_all."nb_order_containing thermometer", 0) "nb_order_containing thermometer",
      COALESCE(b_all."nb_order_containing travel series", 0) "nb_order_containing travel series",
      COALESCE(b_all."nb_order_containing ultrasonic cleaner", 0) "nb_order_containing ultrasonic cleaner",
      COALESCE(b_all."nb_order_containing upper body massager", 0) "nb_order_containing upper body massager",
      COALESCE(b_all."nb_order_containing vacuum cleaner", 0) "nb_order_containing vacuum cleaner",
      COALESCE(b_all."nb_order_containing water purifier", 0) "nb_order_containing water purifier",
      COALESCE(b_all."nb_order_using_birthday_promotion", 0) "nb_order_using_birthday_promotion",

--       COALESCE(b_all."nb_order_containing_attribute habit building", 0) "nb_order_containing_attribute habit building",
--       COALESCE(b_all."nb_order_containing_attribute special occasion", 0) "nb_order_containing_attribute special occasion",
--       COALESCE(b_all."nb_order_containing_attribute discount bargain hunter", 0) "nb_order_containing_attribute discount bargain hunter",
--       COALESCE(b_all."nb_order_containing_attribute luxury", 0) "nb_order_containing_attribute luxury",
--       COALESCE(b_all."nb_order_containing_attribute home", 0) "nb_order_containing_attribute home",
--       COALESCE(b_all."nb_order_containing_attribute travel", 0) "nb_order_containing_attribute travel",
--       COALESCE(b_all."nb_order_containing_attribute athlete", 0) "nb_order_containing_attribute athlete",
--       COALESCE(b_all."nb_order_containing_attribute beauty", 0) "nb_order_containing_attribute beauty",
--       COALESCE(b_all."nb_order_containing_attribute pain relief", 0) "nb_order_containing_attribute pain relief",
--       COALESCE(b_all."nb_order_containing_attribute diet", 0) "nb_order_containing_attribute diet",
--       COALESCE(b_all."nb_order_containing_attribute health", 0) "nb_order_containing_attribute health",
--       COALESCE(b_all."nb_order_containing_attribute light relaxation", 0) "nb_order_containing_attribute light relaxation",
--       COALESCE(b_all."nb_order_containing_attribute children oriented", 0) "nb_order_containing_attribute children oriented",

      b_all."nb_motherday_sales" nb_motherday_sales,
      b_all."nb_close_to_birthday" "nb_close_to_birthday",

      b_all."total_amount_category accessory",
      b_all."total_amount_category air purifier",
      b_all."total_amount_category blood pressure monitor",
      b_all."total_amount_category body composition monitor",
      b_all."total_amount_category eye massager",
      b_all."total_amount_category family hygiene",
      b_all."total_amount_category gel pad series",
      b_all."total_amount_category handheld massager",
      b_all."total_amount_category head massager",
      b_all."total_amount_category humidifier",
      b_all."total_amount_category innovative fitness",
      b_all."total_amount_category leg massager",
      b_all."total_amount_category lower body massager",
      b_all."total_amount_category massage chair",
      b_all."total_amount_category massage sofa",
      b_all."total_amount_category pedometer",
      b_all."total_amount_category pulse massager",
      b_all."total_amount_category sleep well series",
      b_all."total_amount_category slim belts",
      b_all."total_amount_category thermometer",
      b_all."total_amount_category travel series",
      b_all."total_amount_category ultrasonic cleaner",
      b_all."total_amount_category upper body massager",
      b_all."total_amount_category vacuum cleaner",
      b_all."total_amount_category water purifier",

--       b_all."total_amount_attribute habit building",
--       b_all."total_amount_attribute special occasion",
--       b_all."total_amount_attribute discount bargain hunter",
--       b_all."total_amount_attribute luxury",
--       b_all."total_amount_attribute home",
--       b_all."total_amount_attribute travel",
--       b_all."total_amount_attribute athlete",
--       b_all."total_amount_attribute beauty",
--       b_all."total_amount_attribute pain relief",
--       b_all."total_amount_attribute diet",
--       b_all."total_amount_attribute health",
--       b_all."total_amount_attribute light relaxation",
--       b_all."total_amount_attribute children oriented",

      b_all.nb_promo_used,
      b_all.nb_trade_in,
      b_all.nb_vouchers_used,
      b_all.nb_redemption_used,
      b_all.total_discount_from_voucher,
      b_all.nb_freebies,
      b_all.nb_gift_with_purchases,
      b_all.nb_paid_items,
      b_all.nb_paid_products,

      EXTRACT('days' FROM CURRENT_DATE - c.first_order_date::TIMESTAMP) member_for_days,
      EXTRACT('year' FROM c.first_order_date::TIMESTAMP) member_cohort,
      EXTRACT('year' FROM CURRENT_DATE - c.first_order_date::TIMESTAMP) member_for_years,

      b1.main_item_model first_main_item_model,
      b1.main_item_promo_description first_main_item_promo_description,
      b1.main_item_promo_id first_main_item_promo_id,
      b1.main_item_quantity first_main_item_quantity,
      b1.main_item_amount_paid first_main_item_amount_paid,
      b1.main_item_category first_main_item_category,
      b1.main_item_description first_main_item_description,
      b1.main_item_model_description first_main_item_model_description,
      b1.salesperson first_basket_salesperson,
      b1.nb_diff_items first_basket_nb_diff_items,
      b1.total first_basket_total,
      b1.order_date first_basket_order_date,
      b1.nb_promo_used first_basket_nb_promo_used,
      b1.nb_trade_in first_basket_nb_trade_in,
      b1.nb_vouchers_used first_basket_nb_vouchers_used,
      b1.nb_redemption_used first_basket_nb_redemption_used,
      b1.total_discount_from_voucher first_basket_total_discount_from_voucher,
      b1.nb_freebies first_basket_nb_freebies,
      b1.nb_gift_with_purchases first_basket_nb_gift_with_purchases,
      b1.nb_paid_items first_basket_nb_paid_items,
      b1.nb_paid_products first_basket_nb_paid_products,
      b1.has_technical_service first_basket_has_technical_service,
      b1.has_warranty first_basket_has_warranty,
      b1.outlet_code first_basket_outlet_code,
      b1.outlet_type first_basket_outlet_type,

      b2.main_item_model second_main_item_model,
      b2.main_item_promo_description second_main_item_promo_description,
      b2.main_item_promo_id second_main_item_promo_id,
      b2.main_item_quantity second_main_item_quantity,
      b2.main_item_amount_paid second_main_item_amount_paid,
      b2.main_item_category second_main_item_category,
      b2.main_item_description second_main_item_description,
      b2.main_item_model_description second_main_item_model_description,
      b2.salesperson second_basket_salesperson,
      b2.nb_diff_items second_basket_nb_diff_items,
      b2.total second_basket_total,
      b2.order_date second_basket_order_date,
      b2.nb_promo_used second_basket_nb_promo_used,
      b2.nb_trade_in second_basket_nb_trade_in,
      b2.nb_vouchers_used second_basket_nb_vouchers_used,
      b2.nb_redemption_used second_basket_nb_redemption_used,
      b2.total_discount_from_voucher second_basket_total_discount_from_voucher,
      b2.nb_freebies second_basket_nb_freebies,
      b2.nb_gift_with_purchases second_basket_nb_gift_with_purchases,
      b2.nb_paid_items second_basket_nb_paid_items,
      b2.nb_paid_products second_basket_nb_paid_products,
      b2.has_technical_service second_basket_has_technical_service,
      b2.has_warranty second_basket_has_warranty,
      b2.outlet_code second_basket_outlet_code,
      b2.outlet_type second_basket_outlet_type,

      b3.main_item_model third_main_item_model,
      b3.main_item_promo_description third_main_item_promo_description,
      b3.main_item_promo_id third_main_item_promo_id,
      b3.main_item_quantity third_main_item_quantity,
      b3.main_item_amount_paid third_main_item_amount_paid,
      b3.main_item_category third_main_item_category,
      b3.main_item_description third_main_item_description,
      b3.main_item_model_description third_main_item_model_description,
      b3.salesperson third_basket_salesperson,
      b3.nb_diff_items third_basket_nb_diff_items,
      b3.total third_basket_total,
      b3.order_date third_basket_order_date,
      b3.nb_promo_used third_basket_nb_promo_used,
      b3.nb_trade_in third_basket_nb_trade_in,
      b3.nb_vouchers_used third_basket_nb_vouchers_used,
      b3.nb_redemption_used third_basket_nb_redemption_used,
      b3.total_discount_from_voucher third_basket_total_discount_from_voucher,
      b3.nb_freebies third_basket_nb_freebies,
      b3.nb_gift_with_purchases third_basket_nb_gift_with_purchases,
      b3.nb_paid_items third_basket_nb_paid_items,
      b3.nb_paid_products third_basket_nb_paid_products,
      b3.has_technical_service third_basket_has_technical_service,
      b3.has_warranty third_basket_has_warranty,
      b3.outlet_code third_basket_outlet_code,
      b3.outlet_type third_basket_outlet_type,

      b_last.main_item_model last_main_item_model,
      b_last.main_item_promo_description last_main_item_promo_description,
      b_last.main_item_promo_id last_main_item_promo_id,
      b_last.main_item_quantity last_main_item_quantity,
      b_last.main_item_amount_paid last_main_item_amount_paid,
      b_last.main_item_category last_main_item_category,
      b_last.main_item_description last_main_item_description,
      b_last.main_item_model_description last_main_item_model_description,
      b_last.salesperson last_basket_salesperson,
      b_last.nb_diff_items last_basket_nb_diff_items,
      b_last.total last_basket_total,
      b_last.order_date last_basket_order_date,
      b_last.nb_promo_used last_basket_nb_promo_used,
      b_last.nb_trade_in last_basket_nb_trade_in,
      b_last.nb_vouchers_used last_basket_nb_vouchers_used,
      b_last.nb_redemption_used last_basket_nb_redemption_used,
      b_last.total_discount_from_voucher last_basket_total_discount_from_voucher,
      b_last.nb_freebies last_basket_nb_freebies,
      b_last.nb_gift_with_purchases last_basket_nb_gift_with_purchases,
      b_last.nb_paid_items last_basket_nb_paid_items,
      b_last.nb_paid_products last_basket_nb_paid_products,
      b_last.has_technical_service last_basket_has_technical_service,
      b_last.has_warranty last_basket_has_warranty,
      b_last.outlet_code last_basket_outlet_code,
      b_last.outlet_type last_basket_outlet_type,

--       COALESCE(c2010.year_nb_basket, 0) nb_basket_2010,
--       COALESCE(c2010.year_total, 0) basket_total_2010,
--       COALESCE(c2011.year_nb_basket, 0) nb_basket_2011,
--       COALESCE(c2011.year_total, 0) basket_total_2011,
--       COALESCE(c2012.year_nb_basket, 0) nb_basket_2012,
--       COALESCE(c2012.year_total, 0) basket_total_2012,
--       COALESCE(c2013.year_nb_basket, 0) nb_basket_2013,
--       COALESCE(c2013.year_total, 0) basket_total_2013,
--       COALESCE(c2014.year_nb_basket, 0) nb_basket_2014,
--       COALESCE(c2014.year_total, 0) basket_total_2014,
--       COALESCE(c2015.year_nb_basket, 0) nb_basket_2015,
--       COALESCE(c2015.year_total, 0) basket_total_2015,
--       COALESCE(c2016.year_nb_basket, 0) nb_basket_2016,
--       COALESCE(c2016.year_total, 0) basket_total_2016,
--       COALESCE(c2017.year_nb_basket, 0) nb_basket_2017,
--       COALESCE(c2017.year_total, 0) basket_total_2017,
--       COALESCE(c2018.year_nb_basket, 0) nb_basket_2018,
--       COALESCE(c2018.year_total, 0) basket_total_2018,
--
--
--       COALESCE(c0.year_nb_basket, 0) nb_basket_y0,
--       COALESCE(c0.year_total, 0) basket_total_y0,
--       COALESCE(c1.year_nb_basket, 0) nb_basket_y1,
--       COALESCE(c1.year_total, 0) basket_total_y1,
--       COALESCE(c2.year_nb_basket, 0) nb_basket_y2,
--       COALESCE(c2.year_total, 0) basket_total_y2,
--       COALESCE(c3.year_nb_basket, 0) nb_basket_y3,
--       COALESCE(c3.year_total, 0) basket_total_y3,
--       COALESCE(c4.year_nb_basket, 0) nb_basket_y4,
--       COALESCE(c4.year_total, 0) basket_total_y4,
--       COALESCE(c5.year_nb_basket, 0) nb_basket_y5,
--       COALESCE(c5.year_total, 0) basket_total_y5,
--       COALESCE(c6.year_nb_basket, 0) nb_basket_y6,
--       COALESCE(c6.year_total, 0) basket_total_y6,
--       COALESCE(c7.year_nb_basket, 0) nb_basket_y7,
--       COALESCE(c7.year_total, 0) basket_total_y7,
--       COALESCE(c8.year_nb_basket, 0) nb_basket_y8,
--       COALESCE(c8.year_total, 0) basket_total_y8,


      val.big_stuff_value,
      val.small_stuff_value,

      b_last.order_date last_purchase_date,
      ((b_last.order_date - c.first_order_date::date) / 365.0) customership_age,
      CURRENT_DATE - b_last.order_date days_from_last_purchase,
--       (CURRENT_DATE - b_last.order_date) > (365 * 3.5) dead_customer,
      CASE
      WHEN (CURRENT_DATE - b_last.order_date) > (365 * 3.5)
        THEN
          b_last.order_date + INTERVAL '1277 days'
        ELSE
          NULL
      END date_of_customer_death,


      b_last.main_item_category IN ('Massage Chair', 'Massage Sofa') has_massage_chair_last_purchase,

      second_visit.days_since_last_purchase days_to_second_purchase,

      three_point_five.total clv_3_5_years,

      -- RFM
      CASE WHEN nb_paid_baskets > 1 THEN 'High' ELSE 'Average' END frequency,
      CASE WHEN CURRENT_DATE - b_last.order_date < 366
        THEN 'Recent'
      ELSE
        CASE WHEN CURRENT_DATE - b_last.order_date < 365*3.5
          THEN 'Average'
        ELSE 'Inactive'
        END
      END recency,

      -- E.P.: updated 20190117, by demand from ENGAGE
      CASE WHEN b_paid.total_value < 2000 THEN 'Low'
        ELSE CASE WHEN b_paid.total_value < 4000 THEN 'Mid'
          ELSE CASE WHEN b_paid.total_value < 8000 THEN 'High'
            ELSE CASE WHEN b_paid.total_value < 10000 THEN 'VIP'
              ELSE 'VVIP' END
          END
        END
      END monetary_value

--       CASE WHEN b_paid.total_value < 1500 THEN 'Low'
--         ELSE CASE WHEN b_paid.total_value < 3599 THEN 'High'
--           ELSE 'Very High'
--           END
--         END monetary_value


    FROM import.engage$customers c
    LEFT JOIN (
        SELECT
          customer_id,
          AVG(days_since_last_purchase) average_days_between_purchases,
          AVG(total) average_purchase_total,
          SUM(total) total_value,
          COUNT(order_id) nb_paid_baskets,
          MAX(total) max_paid_basket_total,

          SUM(is_motherday_sales::int) "nb_motherday_purchase",
          SUM(close_to_birthday::int) "nb_purchase_close_to_birthday"

        FROM public.engage$baskets
        WHERE total > 0
        GROUP BY customer_id
    ) b_paid
    ON b_paid.customer_id = c.customer_id

    LEFT JOIN (
        SELECT
          customer_id,

          AVG(days_since_last_purchase) average_days_between_baskets,
          COUNT(order_id) nb_baskets,
          SUM("has_category accessory"::int) "nb_order_containing accessory",
          SUM("has_category air purifier"::int) "nb_order_containing air purifier",
          SUM("has_category blood pressure monitor"::int) "nb_order_containing blood pressure monitor",
          SUM("has_category body composition monitor"::int) "nb_order_containing body composition monitor",
          SUM("has_category eye massager"::int) "nb_order_containing eye massager",
          SUM("has_category family hygiene"::int) "nb_order_containing family hygiene",
          SUM("has_category gel pad series"::int) "nb_order_containing gel pad series",
          SUM("has_category handheld massager"::int) "nb_order_containing handheld massager",
          SUM("has_category head massager"::int) "nb_order_containing head massager",
          SUM("has_category humidifier"::int) "nb_order_containing humidifier",
          SUM("has_category innovative fitness"::int) "nb_order_containing innovative fitness",
          SUM("has_category leg massager"::int) "nb_order_containing leg massager",
          SUM("has_category lower body massager"::int) "nb_order_containing lower body massager",
          SUM("has_category massage chair"::int) "nb_order_containing massage chair",
          SUM("has_category massage sofa"::int) "nb_order_containing massage sofa",
          SUM("has_category pedometer"::int) "nb_order_containing pedometer",
          SUM("has_category pulse massager"::int) "nb_order_containing pulse massager",
          SUM("has_category sleep well series"::int) "nb_order_containing sleep well series",
          SUM("has_category slim belts"::int) "nb_order_containing slim belts",
          SUM("has_category thermometer"::int) "nb_order_containing thermometer",
          SUM("has_category travel series"::int) "nb_order_containing travel series",
          SUM("has_category ultrasonic cleaner"::int) "nb_order_containing ultrasonic cleaner",
          SUM("has_category upper body massager"::int) "nb_order_containing upper body massager",
          SUM("has_category vacuum cleaner"::int) "nb_order_containing vacuum cleaner",
          SUM("has_category water purifier"::int) "nb_order_containing water purifier",
          SUM("has_birthday_promotion"::int) "nb_order_using_birthday_promotion",

--           SUM("has_attribute habit building"::int) "nb_order_containing_attribute habit building",
--           SUM("has_attribute special occasion"::int) "nb_order_containing_attribute special occasion",
--           SUM("has_attribute discount bargain hunter"::int) "nb_order_containing_attribute discount bargain hunter",
--           SUM("has_attribute luxury"::int) "nb_order_containing_attribute luxury",
--           SUM("has_attribute home"::int) "nb_order_containing_attribute home",
--           SUM("has_attribute travel"::int) "nb_order_containing_attribute travel",
--           SUM("has_attribute athlete"::int) "nb_order_containing_attribute athlete",
--           SUM("has_attribute beauty"::int) "nb_order_containing_attribute beauty",
--           SUM("has_attribute pain relief"::int) "nb_order_containing_attribute pain relief",
--           SUM("has_attribute diet"::int) "nb_order_containing_attribute diet",
--           SUM("has_attribute health"::int) "nb_order_containing_attribute health",
--           SUM("has_attribute light relaxation"::int) "nb_order_containing_attribute light relaxation",
--           SUM("has_attribute children oriented"::int) "nb_order_containing_attribute children oriented",

          SUM("total_amount_category accessory") "total_amount_category accessory",
          SUM("total_amount_category air purifier") "total_amount_category air purifier",
          SUM("total_amount_category blood pressure monitor") "total_amount_category blood pressure monitor",
          SUM("total_amount_category body composition monitor") "total_amount_category body composition monitor",
          SUM("total_amount_category eye massager") "total_amount_category eye massager",
          SUM("total_amount_category family hygiene") "total_amount_category family hygiene",
          SUM("total_amount_category gel pad series") "total_amount_category gel pad series",
          SUM("total_amount_category handheld massager") "total_amount_category handheld massager",
          SUM("total_amount_category head massager") "total_amount_category head massager",
          SUM("total_amount_category humidifier") "total_amount_category humidifier",
          SUM("total_amount_category innovative fitness") "total_amount_category innovative fitness",
          SUM("total_amount_category leg massager") "total_amount_category leg massager",
          SUM("total_amount_category lower body massager") "total_amount_category lower body massager",
          SUM("total_amount_category massage chair") "total_amount_category massage chair",
          SUM("total_amount_category massage sofa") "total_amount_category massage sofa",
          SUM("total_amount_category pedometer") "total_amount_category pedometer",
          SUM("total_amount_category pulse massager") "total_amount_category pulse massager",
          SUM("total_amount_category sleep well series") "total_amount_category sleep well series",
          SUM("total_amount_category slim belts") "total_amount_category slim belts",
          SUM("total_amount_category thermometer") "total_amount_category thermometer",
          SUM("total_amount_category travel series") "total_amount_category travel series",
          SUM("total_amount_category ultrasonic cleaner") "total_amount_category ultrasonic cleaner",
          SUM("total_amount_category upper body massager") "total_amount_category upper body massager",
          SUM("total_amount_category vacuum cleaner") "total_amount_category vacuum cleaner",
          SUM("total_amount_category water purifier") "total_amount_category water purifier",

--           SUM("total_amount_attribute habit building") "total_amount_attribute habit building",
--           SUM("total_amount_attribute special occasion") "total_amount_attribute special occasion",
--           SUM("total_amount_attribute discount bargain hunter") "total_amount_attribute discount bargain hunter",
--           SUM("total_amount_attribute luxury") "total_amount_attribute luxury",
--           SUM("total_amount_attribute home") "total_amount_attribute home",
--           SUM("total_amount_attribute travel") "total_amount_attribute travel",
--           SUM("total_amount_attribute athlete") "total_amount_attribute athlete",
--           SUM("total_amount_attribute beauty") "total_amount_attribute beauty",
--           SUM("total_amount_attribute pain relief") "total_amount_attribute pain relief",
--           SUM("total_amount_attribute diet") "total_amount_attribute diet",
--           SUM("total_amount_attribute health") "total_amount_attribute health",
--           SUM("total_amount_attribute light relaxation") "total_amount_attribute light relaxation",
--           SUM("total_amount_attribute children oriented") "total_amount_attribute children oriented",

          SUM(is_motherday_sales::int) "nb_motherday_sales",
          SUM(close_to_birthday::int) "nb_close_to_birthday",

          SUM(nb_promo_used) nb_promo_used,
          SUM(nb_trade_in) nb_trade_in,
          SUM(nb_vouchers_used) nb_vouchers_used,
          SUM(nb_redemption_used) nb_redemption_used,
          SUM(total_discount_from_voucher) total_discount_from_voucher,
          SUM(nb_freebies) nb_freebies,
          SUM(nb_gift_with_purchases) nb_gift_with_purchases,
          SUM(nb_paid_items) nb_paid_items,
          SUM(nb_paid_products) nb_paid_products

        FROM public.engage$baskets
        WHERE total > 0
        GROUP BY customer_id
    ) b_all
    ON b_all.customer_id = c.customer_id

    -- First, second, third, last basket
    LEFT JOIN engage$baskets b1
    ON b1.customer_id = c.customer_id AND b1.nth_paid_basket = 1
    LEFT JOIN engage$baskets b2
    ON b2.customer_id = c.customer_id AND b2.nth_paid_basket = 2
    LEFT JOIN engage$baskets b3
    ON b3.customer_id = c.customer_id AND b3.nth_paid_basket = 3
    LEFT JOIN engage$baskets b_last
    ON b_last.customer_id = c.customer_id AND b_last.nth_paid_basket_from_last = 1

--     -- stats per calendar year
--     LEFT JOIN _customership_calendar c2010
--     ON c2010.customer_id = c.customer_id AND c2010.calendar_year = 2010
--     LEFT JOIN _customership_calendar c2011
--     ON c2011.customer_id = c.customer_id AND c2011.calendar_year = 2011
--     LEFT JOIN _customership_calendar c2012
--     ON c2012.customer_id = c.customer_id AND c2012.calendar_year = 2012
--     LEFT JOIN _customership_calendar c2013
--     ON c2013.customer_id = c.customer_id AND c2013.calendar_year = 2013
--     LEFT JOIN _customership_calendar c2014
--     ON c2014.customer_id = c.customer_id AND c2014.calendar_year = 2014
--     LEFT JOIN _customership_calendar c2015
--     ON c2015.customer_id = c.customer_id AND c2015.calendar_year = 2015
--     LEFT JOIN _customership_calendar c2016
--     ON c2016.customer_id = c.customer_id AND c2016.calendar_year = 2016
--     LEFT JOIN _customership_calendar c2017
--     ON c2017.customer_id = c.customer_id AND c2017.calendar_year = 2017
--     LEFT JOIN _customership_calendar c2018
--     ON c2017.customer_id = c.customer_id AND c2017.calendar_year = 2018
--
--     -- stats per membership years
--     LEFT JOIN _customership_yearly c0
--     ON c0.customer_id = c.customer_id AND c0.customership_year = 0
--     LEFT JOIN _customership_yearly c1
--     ON c1.customer_id = c.customer_id AND c1.customership_year = 1
--     LEFT JOIN _customership_yearly c2
--     ON c2.customer_id = c.customer_id AND c2.customership_year = 2
--     LEFT JOIN _customership_yearly c3
--     ON c3.customer_id = c.customer_id AND c3.customership_year = 3
--     LEFT JOIN _customership_yearly c4
--     ON c4.customer_id = c.customer_id AND c4.customership_year = 4
--     LEFT JOIN _customership_yearly c5
--     ON c5.customer_id = c.customer_id AND c5.customership_year = 5
--     LEFT JOIN _customership_yearly c6
--     ON c6.customer_id = c.customer_id AND c6.customership_year = 6
--     LEFT JOIN _customership_yearly c7
--     ON c6.customer_id = c.customer_id AND c6.customership_year = 7
--     LEFT JOIN _customership_yearly c8
--     ON c6.customer_id = c.customer_id AND c6.customership_year = 8

    LEFT JOIN _customer_value val
    ON val.customer_id = c.customer_id

    -- number of chairs bought in the past 2 years
    LEFT JOIN (
      SELECT
        customer_id,
        COUNT(order_id) order_count
      FROM engage$baskets
      WHERE order_date > current_date - (365 * 2)
      AND main_item_category IN ('Massage Chair', 'Massage Sofa')
      GROUP BY customer_id
    ) bought_chair_3_years_ago
    ON bought_chair_3_years_ago.customer_id = c.customer_id

    LEFT JOIN (
        SELECT
          customer_id,
          days_since_last_purchase
        FROM engage$baskets
        WHERE nth_basket = 2
    ) second_visit
    ON second_visit.customer_id = c.customer_id

    LEFT JOIN (
        SELECT
          c.customer_id,
          sum(total) total
        FROM import.engage$customers c
        LEFT JOIN engage$baskets b
        ON b.customer_id = c.customer_id
        AND b.order_date < c.first_order_date + '1278 days'
        GROUP BY c.customer_id
    ) three_point_five
    ON c.customer_id = three_point_five.customer_id


    WHERE COALESCE(b_paid.nb_paid_baskets, 0) > 0
);


DROP TABLE IF EXISTS engage$customer_this_period;
CREATE TABLE engage$customer_this_period AS (
    SELECT
      c.customer_id,
      SUM(ts.total_amount_with_tax) revenue_this_year,
      COUNT(DISTINCT(b.order_id)) nb_paid_baskets_this_year,
      SUM(ts_this_month.total_amount_with_tax) revenue_this_month,
      COUNT(DISTINCT(b_this_month.order_id)) nb_paid_baskets_this_month,

      SUM(ts_last_year_all.total_amount_with_tax) revenue_last_year_all,
      COUNT(DISTINCT(b_last_year_all.order_id)) nb_paid_baskets_last_year_all,

      SUM(ts_last_year_ytd.total_amount_with_tax) revenue_last_year_ytd,
      COUNT(DISTINCT(b_last_year_ytd.order_id)) nb_paid_baskets_last_year_ytd

    FROM engage$customers c
    LEFT JOIN _engage$transactions ts
      ON c.customer_id = ts.customer_id
         AND EXTRACT(YEAR FROM ts.order_date) = EXTRACT(YEAR FROM current_date)
    LEFT JOIN engage$baskets b
      ON b.customer_id = c.customer_id
         AND EXTRACT(YEAR FROM b.order_date) = EXTRACT(YEAR FROM current_date)
         AND b.total > 0

    LEFT JOIN _engage$transactions ts_this_month
      ON c.customer_id = ts_this_month.customer_id
         AND EXTRACT(YEAR FROM ts_this_month.order_date) = EXTRACT(YEAR FROM current_date)
         AND EXTRACT(MONTH FROM ts_this_month.order_date) = EXTRACT(MONTH FROM current_date)
    LEFT JOIN engage$baskets b_this_month
      ON b_this_month.customer_id = c.customer_id
         AND EXTRACT(YEAR FROM b_this_month.order_date) = EXTRACT(YEAR FROM current_date)
         AND EXTRACT(MONTH FROM b_this_month.order_date) = EXTRACT(MONTH FROM current_date)
         AND b_this_month.total > 0

    LEFT JOIN _engage$transactions ts_last_year_all
      ON c.customer_id = ts_last_year_all.customer_id
         AND EXTRACT(YEAR FROM ts_last_year_all.order_date) = EXTRACT(YEAR FROM current_date) - 1
    LEFT JOIN engage$baskets b_last_year_all
      ON b_last_year_all.customer_id = c.customer_id
         AND EXTRACT(YEAR FROM b_last_year_all.order_date) = EXTRACT(YEAR FROM current_date) - 1
         AND b_last_year_all.total > 0

    LEFT JOIN _engage$transactions ts_last_year_ytd
      ON c.customer_id = ts_last_year_ytd.customer_id
         AND EXTRACT(YEAR FROM ts_last_year_ytd.order_date) = EXTRACT(YEAR FROM current_date) - 1
         AND EXTRACT(MONTH FROM ts_last_year_ytd.order_date) = EXTRACT(MONTH FROM current_date)
         AND EXTRACT(DAY FROM ts_last_year_ytd.order_date) <= EXTRACT(DAY FROM current_date)
    LEFT JOIN engage$baskets b_last_year_ytd
      ON b_last_year_ytd.customer_id = c.customer_id
         AND EXTRACT(YEAR FROM b_last_year_ytd.order_date) = EXTRACT(YEAR FROM current_date) - 1
         AND EXTRACT(MONTH FROM b_last_year_ytd.order_date) = EXTRACT(MONTH FROM current_date)
         AND EXTRACT(DAY FROM b_last_year_ytd.order_date) <= EXTRACT(DAY FROM current_date)
         AND b_last_year_ytd.total > 0


    GROUP BY c.customer_id
);

------------------------------------------------------------------------------------------------------------------------
-- TRANSACTIONS
------------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS public.engage$transactions;
CREATE TABLE public.engage$transactions
    DISTKEY(order_id) SORTKEY(order_id, order_date)
AS (
    SELECT
      ts.*,
      b.days_since_last_purchase,
      b.days_to_next_purchase,
      b.close_to_birthday
    FROM _engage$transactions ts
    LEFT JOIN (SELECT order_id, days_since_last_purchase, days_to_next_purchase, close_to_birthday FROM public.engage$baskets) b
    ON b.order_id = ts.order_id
);


DROP TABLE IF EXISTS public.engage$product_sold;
CREATE TABLE public.engage$product_sold AS (
    SELECT
      item_no,
      order_date,
      order_id,
      customer_id,
      item_quantity,
      total_amount_with_tax,
      item_model,
      item_category,
      item_description,
      item_model_description,
      promo_id,
      promo_description,
      outlet_code,
      is_returning_customer

    FROM public.engage$transactions

    WHERE item_quantity > 0
--     AND completed IS TRUE
    AND item_type IN ('TSS', 'PRODUCTS', 'ACCESSORIES')
    AND total_amount_with_tax > 0

);

DROP TABLE IF EXISTS public.engage$outlets;
CREATE TABLE public.engage$outlets AS (
    SELECT * FROM import.engage$outlets
);

DROP TABLE IF EXISTS public.engage$promos;
CREATE TABLE public.engage$promos AS (
    SELECT * FROM import.engage$promos
);

DROP TABLE IF EXISTS public.engage$items;
CREATE TABLE public.engage$items AS (
    SELECT
      item_no,
      item_category,
      item_description,
      item_group,
      item_model,
      item_model_description,
      item_type
    FROM
      (SELECT
        item_no,
        item_category,
        item_description,
        item_group,
        item_model,
        item_model_description,
        item_type,
        row_number() OVER (PARTITION BY item_no ORDER BY item_category, item_group) pos
       FROM import.engage$items ) x
    WHERE x.pos = 1
);


------------------------------------------------------------------------------------------------------------------------
---  TESTS
------------------------------------------------------------------------------------------------------------------------

-- Check that customers id are unique
SELECT
  assert(count(distinct(customer_id)) = count(*), 'Problem with customer_id uniqueness')
FROM public.engage$customers;

-- Unique items only
SELECT
  assert(count(distinct(item_no)) = count(*), 'Duplicate item_no in engage$items table')
FROM public.engage$items;

-- Check that baskets id are unique
SELECT
  assert(count(distinct(order_id)) = count(*), 'Problem with order_id uniqueness')
FROM public.engage$baskets;

-- Check that the sum of all baskets is the same as the sum for all transactions
SELECT
  assert(abs(b.total - s.total) < 0.001, 'Mismatch: sum of all baskets vs. sum of all sales')
FROM
  (SELECT SUM(total) total FROM engage$baskets) b
LEFT JOIN
  (SELECT SUM(total_amount_with_tax) total FROM engage$transactions) s
  ON TRUE;

-- Check that all the campaign id are unique
SELECT
  assert(count(distinct(campaign_id)) = count(*), 'Problem with campaign_id uniqueness')
FROM public.engage$campaigns;

-- Check no campaign_id / item_model duplicates
SELECT
  assert(
      (SELECT COUNT(*) FROM (SELECT DISTINCT campaign_id, item_model FROM public.engage$campaign_item_models) _) =
      (SELECT COUNT(*) FROM engage$campaign_item_models),
      'Problem with campaign <-> item_model uniqueness'
  );
