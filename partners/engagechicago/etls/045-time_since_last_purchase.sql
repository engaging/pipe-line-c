-- 045-time_since_last_purchase.sql
-- Miscellaneous transformation

--
-- Add "time since last purchase X" for each customer / item_category
--
DROP TABLE IF EXISTS _latest_purchase;
CREATE TEMPORARY TABLE _latest_purchase AS (
      SELECT
        customer_id,
        MAX(order_date) order_date,
        item_category
      FROM engage$transactions
      WHERE item_category IS NOT NULL
      GROUP BY customer_id, item_category
);

DROP TABLE IF EXISTS engage$time_since_last_purchase;
CREATE TABLE engage$time_since_last_purchase AS (
    SELECT
      c.customer_id,

      COALESCE(current_date - p_chair.order_date, 9999) time_since_last_purchase_chair,
      COALESCE(current_date - p_sofa.order_date, 9999)  time_since_last_purchase_sofa,
      COALESCE(current_date - p_cat1.order_date, 9999)  time_since_last_purchase_upper_body,
      COALESCE(current_date - p_cat2.order_date, 9999)  time_since_last_purchase_humidifier,
      COALESCE(current_date - p_cat3.order_date, 9999)  time_since_last_purchase_eye_massager,
      COALESCE(current_date - p_cat4.order_date, 9999)  time_since_last_purchase_air_purifier,
      COALESCE(current_date - p_cat5.order_date, 9999)  time_since_last_purchase_handheld_massager,
      COALESCE(current_date - p_cat6.order_date, 9999)  time_since_last_purchase_head_massager,
      COALESCE(current_date - p_cat7.order_date, 9999)  time_since_last_purchase_leg_massager,
      COALESCE(current_date - p_cat8.order_date, 9999)  time_since_last_purchase_travel_series,
      COALESCE(current_date - p_cat9.order_date, 9999)  time_since_last_purchase_lower_body,
      
      COALESCE(p_chair.order_date, date '2009-01-01') date_of_last_purchase_chair,
      COALESCE(p_sofa.order_date, date '2009-01-01') date_of_last_purchase_sofa,
      COALESCE(p_cat1.order_date, date '2009-01-01') date_of_last_purchase_upper_body,
      COALESCE(p_cat2.order_date, date '2009-01-01') date_of_last_purchase_humidifier,
      COALESCE(p_cat3.order_date, date '2009-01-01') date_of_last_purchase_eye_massager,
      COALESCE(p_cat4.order_date, date '2009-01-01') date_of_last_purchase_air_purifier,
      COALESCE(p_cat5.order_date, date '2009-01-01') date_of_last_purchase_handheld_massager,
      COALESCE(p_cat6.order_date, date '2009-01-01') date_of_last_purchase_head_massager,
      COALESCE(p_cat7.order_date, date '2009-01-01') date_of_last_purchase_leg_massager,
      COALESCE(p_cat8.order_date, date '2009-01-01') date_of_last_purchase_travel_series,
      COALESCE(p_cat9.order_date, date '2009-01-01') date_of_last_purchase_lower_body

    FROM
      engage$customers c

    LEFT JOIN _latest_purchase p_chair
      ON p_chair.item_category = 'Massage Chair'
     AND p_chair.customer_id = c.customer_id

    LEFT JOIN _latest_purchase p_sofa
    ON p_sofa.customer_id = c.customer_id
    AND p_sofa.item_category = 'Massage Sofa'

    LEFT JOIN _latest_purchase p_cat1
    ON p_cat1.customer_id = c.customer_id
    AND p_cat1.item_category = 'Upper Body Massager'

    LEFT JOIN _latest_purchase p_cat2
    ON p_cat2.customer_id = c.customer_id
    AND p_cat2.item_category = 'Humidifier'

    LEFT JOIN _latest_purchase p_cat3
    ON p_cat3.customer_id = c.customer_id
    AND p_cat3.item_category = 'Eye Massager'

    LEFT JOIN _latest_purchase p_cat4
    ON p_cat4.customer_id = c.customer_id
    AND p_cat4.item_category = 'Air Purifier'

    LEFT JOIN _latest_purchase p_cat5
    ON p_cat5.customer_id = c.customer_id
    AND p_cat5.item_category = 'Handheld Massager'

    LEFT JOIN _latest_purchase p_cat6
    ON p_cat6.customer_id = c.customer_id
    AND p_cat6.item_category = 'Head Massager'

    LEFT JOIN _latest_purchase p_cat7
    ON p_cat7.customer_id = c.customer_id
    AND p_cat7.item_category = 'Leg Massager'

    LEFT JOIN _latest_purchase p_cat8
    ON p_cat8.customer_id = c.customer_id
    AND p_cat8.item_category = 'Travel Series'

    LEFT JOIN _latest_purchase p_cat9
    ON p_cat9.customer_id = c.customer_id
    AND p_cat9.item_category = 'Lower Body Massager'
);
