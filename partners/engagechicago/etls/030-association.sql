
DROP TABLE IF EXISTS _upgrades;
CREATE TEMPORARY TABLE _upgrades AS (
    SELECT
      base.*,
      upgrade.order_date - base.order_date days_to_upgrade,
      upgrade.item_category upgrade_item_category,
      upgrade.item_model upgrade_item_model
    FROM engage$product_sold base
    LEFT JOIN engage$product_sold upgrade
           ON base.customer_id = upgrade.customer_id
          AND upgrade.order_date > base.order_date
          AND upgrade.item_category IS NOT NULL
    WHERE base.item_category IS NOT NULL
      AND upgrade.item_category IS NOT NULL
);

DROP TABLE IF EXISTS _support_1;
CREATE TEMPORARY TABLE _support_1 AS (
    SELECT
      item_category,
      COUNT(customer_id) nb_occurrences,
      COUNT(customer_id)::float / (SELECT COUNT(*) FROM _upgrades) support
    FROM
      _upgrades
    GROUP BY _upgrades.item_category
);

DROP TABLE IF EXISTS _support_2;
CREATE TEMPORARY TABLE _support_2 AS (
    SELECT
      item_category,
      upgrade_item_category,
      avg(days_to_upgrade) avg_days_to_upgrade,
      COUNT(customer_id) nb_occurrences,
      COUNT(customer_id)::float / (SELECT COUNT(*) FROM _upgrades) support
    FROM _upgrades
    GROUP BY item_category, upgrade_item_category
);

DROP TABLE IF EXISTS engage$arules;
CREATE TABLE engage$arules AS (
    SELECT
      _support_2.item_category,
      _support_2.upgrade_item_category,
      _support_2.avg_days_to_upgrade,

      _support_2.nb_occurrences,

      s_x.support support_base,
      s_y.support support_upgrade,

      s_x.nb_occurrences x_nb_occurrences,
      s_y.nb_occurrences y_nb_occurrences,

      _support_2.support support_base_upgrade,

      _support_2.support::float / s_x.support confidence,
      _support_2.support::float / (s_x.support * s_y.support) lift
    FROM _support_2

    LEFT JOIN _support_1 s_x
    ON _support_2.item_category = s_x.item_category

    LEFT JOIN _support_1 s_y
    ON _support_2.upgrade_item_category = s_y.item_category
);


------------------------------------------------------------------------------------------------------------------------
-- Looking at Item Category -> Item Category
------------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS customer_journey_support_x;
CREATE TEMPORARY TABLE customer_journey_support_x AS (
    SELECT
      first_main_item_category first_main_item_category,
      COUNT(customer_id) nb_occurrences

    FROM engage$customers
    WHERE first_main_item_category IS NOT NULL
    GROUP BY first_main_item_category

);

DROP TABLE IF EXISTS customer_journey_support_y;
CREATE TEMPORARY TABLE customer_journey_support_y AS (
    SELECT
      coalesce(second_main_item_category, '(no upgrade)') second_main_item_category,
      COUNT(customer_id) nb_occurrences

    FROM engage$customers
    GROUP BY second_main_item_category
);

DROP TABLE IF EXISTS engage$customer_journey;
CREATE TABLE engage$customer_journey AS (

    SELECT
      c.first_main_item_category p1,
      c.second_main_item_category p2,
      x.nb_occurrences nb_occurrences_p1,
      y.nb_occurrences nb_occurrences_p2,
      COUNT(c.customer_id)::float nb_occurrences_p1_p2,

      x.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) support_p1,
      y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) support_p2,
      COUNT(c.customer_id)::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) support_p1_p2,
      (COUNT(c.customer_id)::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) / (x.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) confidence,
      (COUNT(c.customer_id)::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) / ((x.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) * (y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers))) lift,

      -- P(Y|X)
      COUNT(c.customer_id)::float / x.nb_occurrences p_p2_knowing_p1,
      y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) p_p2,
      ((COUNT(c.customer_id)::float / x.nb_occurrences) - y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) ::float / (y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) uplift

--       COUNT(DISTINCT(c2.customer_id)) / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) p_p2_as_any_purchase

    FROM engage$customers c
    LEFT JOIN customer_journey_support_x x
    ON c.first_main_item_category = x.first_main_item_category

    LEFT JOIN customer_journey_support_y y
    ON coalesce(c.second_main_item_category, '(no upgrade)') = y.second_main_item_category

    WHERE c.first_main_item_category IS NOT NULL

--     LEFT JOIN engage$product_sold c2
--     ON c.second_main_item_category = c2.item_category

    GROUP BY c.first_main_item_category, c.second_main_item_category, x.nb_occurrences, y.nb_occurrences
);


------------------------------------------------------------------------------------------------------------------------
-- Looking at item model -> item category
------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS customer_journey_support_model_x;
CREATE TEMPORARY TABLE customer_journey_support_model_x AS (
    SELECT
      first_main_item_model first_main_item_model,
      COUNT(customer_id) nb_occurrences

    FROM engage$customers

    WHERE first_main_item_model IS NOT NULL
    GROUP BY first_main_item_model
);

--
-- the "y" table already exists from before
--


DROP TABLE IF EXISTS engage$customer_journey_model_to_category;
CREATE TABLE engage$customer_journey_model_to_category AS (

    SELECT
      c.first_main_item_model p1,
      coalesce(c.second_main_item_category, '(no upgrade)') p2,
      i.item_model_description,
      x.nb_occurrences nb_occurrences_p1,
      y.nb_occurrences nb_occurrences_p2,

      x.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) support_p1,
      y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) support_p2,
      COUNT(c.customer_id)::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) support_p1_p2,
      (COUNT(c.customer_id)::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) / (x.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) confidence,
      (COUNT(c.customer_id)::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) / ((x.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) * (y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers))) lift,

      -- P(Y|X)
      COUNT(c.customer_id)::float / x.nb_occurrences p_p2_knowing_p1,
      y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) p_p2,
      ((COUNT(c.customer_id)::float / x.nb_occurrences) - y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) ::float / (y.nb_occurrences::float / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers)) uplift

--       COUNT(DISTINCT(c2.customer_id)) / (SELECT COUNT(DISTINCT(customer_id)) nb_customers FROM engage$customers) p_p2_as_any_purchase

    FROM engage$customers c
    LEFT JOIN customer_journey_support_model_x x
    ON c.first_main_item_model = x.first_main_item_model

    LEFT JOIN customer_journey_support_y y
    ON coalesce(c.second_main_item_category, '(no upgrade)') = y.second_main_item_category

    LEFT JOIN public.engage$items i
    ON i.item_model = c.first_main_item_model

    WHERE c.first_main_item_model IS NOT NULL

--     LEFT JOIN engage$product_sold c2
--     ON c.second_main_item_category = c2.item_category

    GROUP BY c.first_main_item_model, c.second_main_item_category, x.nb_occurrences, y.nb_occurrences, i.item_model_description
);

