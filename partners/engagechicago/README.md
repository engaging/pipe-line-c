# ENGAGE Chicago

Pulls data from the ENGAGE FTP server (whole dataset everytime) and pushes
it to S3/Redshift.

Post-hooks: refresh PowerBI dashboard, push data to ENGAGE Capillory instance,
although they don't use it at this time.
