"""
Base class for a partner/vendor pipeline
"""

from abc import ABC, abstractmethod
import configparser
from engagelib.aws.s3 import S3
import logging
import engagelib.aws.redshift as redshift
import engagelib.aws.sns as sns
import engagelib.microsoft.powerbi as powerbi
import time
import traceback


class PartnerPipeline(ABC):
    name = "partner_pipeline"
    long_name = "Unnamed Data Ingestion Pipeline"

    def __init__(self, ini, force_refresh=False):
        self.ini = ini
        self.cfg = configparser.ConfigParser()
        self.cfg.read(ini)
        self.updated = False
        self.force_refresh = force_refresh
        self.s3 = S3.from_config(bucket=self.cfg.get('pipeline', 's3_bucket'),
                                 ini=ini,
                                 inisection='aws')
        self.s3_raw_prefix = self.cfg.get('pipeline', 'bucket_raw_prefix')
        if not self.s3_raw_prefix.endswith('/'):
            self.s3_raw_prefix += '/'
        self.s3_norm_prefix = self.cfg.get('pipeline', 'bucket_normalized_prefix')
        if not self.s3_norm_prefix.endswith('/'):
            self.s3_norm_prefix += '/'
        self.sns = sns.SNS.from_config(ini, arn=self.cfg.get('pipeline', 'topic_arn'))
        self.logger = logging.getLogger('pipeline.{}'.format(self.name))

    @abstractmethod
    def sync_src_raw(self):
        pass

    @abstractmethod
    def sync_raw_norm(self):
        pass

    @abstractmethod
    def sync_to_redshift(self):
        pass

    @abstractmethod
    def update_models(self):
        pass

    def run_etls(self):
        connection = self.get_redshift_connection()
        redshift.run_etls(self.cfg.get('redshift', 'etls'), connection, query_parameters={
            'aws_region': self.cfg.get('aws', 'region'),
            'iam_role': self.cfg.get('redshift', 'iam_role')
        })

    def prepare_dashboard(self):
        to_refresh = self.cfg.get('powerbi', 'to_refresh', fallback='').split(',')
        if len(to_refresh) > 0:
            self.logger.info("Refreshing {} datasets...".format(len(to_refresh)))
            pbi = powerbi.PowerBI(
                application_id=self.cfg.get('powerbi', 'application_id'),
                application_key=self.cfg.get('powerbi', 'application_key'),
                user_id=self.cfg.get('powerbi', 'user_id'),
                user_password=self.cfg.get('powerbi', 'user_password'),
                resource=self.cfg.get('powerbi', 'resource')
            )
            for t in to_refresh:
                try:
                    group_id, dataset_id = t.split('::')
                except ValueError:
                    raise ValueError("Error parsing group_id::dataset_id from config file.")
                self.logger.info("Refreshing dataset {} / {}".format(group_id, dataset_id))
                res = pbi.refresh_dataset(group_id, dataset_id)
                assert res.status_code == 202, "Error refreshing dataset {} in group {} ({}: {!r})".format(dataset_id, group_id, res.status_code, res.text)

    def run_all(self):
        begin = time.time()
        try:
            self.sync_src_raw()
            self.sync_raw_norm()
            if self.updated or self.force_refresh:
                self.sync_to_redshift()
                self.run_etls()
                self.update_models()
                self.prepare_dashboard()
                self.post_refresh_hooks()
                self.sns.publish(subject="{}: Success".format(self.long_name),
                                 message='Operation performed in {:.2f}s'.format(time.time() - begin))
            self.logger.info("pipeline for {} finished".format(self.long_name))
        except Exception as e:
            tb = traceback.format_exc()
            message = "There was an error importing daily data ({}).\n" \
                      "----------------------------------------\n" \
                      "{}\n" \
                      "{}".format(self.long_name, e, tb)
            self.sns.publish(subject="Data Ingestion Pipeline Error", message=message)
            raise e

    def complete_src_to_raw(self, basename):
        """Mark a data import process as complete by uploading a dummy SUCCESS key."""
        self.s3.put_object(obj=b'', s3path=self.s3_raw_prefix + basename + '/' + 'SUCCESS')

    def complete_raw_to_norm(self, basename):
        """Mark a data import process as complete by uploading a dummy SUCCESS key."""
        self.logger.debug("MARKING SUCCESS, TO {}".format(self.s3_norm_prefix + basename + '/' + 'SUCCESS'))
        self.s3.put_object(obj=b'', s3path=self.s3_norm_prefix + basename + '/' + 'SUCCESS')

    def post_refresh_hooks(self):
        pass

    def get_redshift_connection(self):
        return redshift.get_connection(
            user=self.cfg.get('redshift', 'user'),
            dbname=self.cfg.get('redshift', 'database'),
            port=self.cfg.get('redshift', 'port', fallback='5439'),
            host=self.cfg.get('redshift', 'host'),
            password=self.cfg.get('redshift', 'password')
        )
