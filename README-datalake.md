# Engaging Datalake

Store all the data the goes in our pipelines.

The `raw` folder is for data coming from clients with little or no modification.

The `clean` folder is for normalize data that i ready to import to Redshift transparently. This folder can be deleted and
will be re-generated identically from the `raw` folder (not that it _should_ be deleted).

The `static` folder is for support files that do not evolve in time / are not pulled externally.
 
 