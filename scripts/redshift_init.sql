
BEGIN;

-- Revoke all permissions from the PUBLIC group.
REVOKE TEMPORARY ON DATABASE transactions FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE EXECUTE ON FUNCTIONS FROM PUBLIC;

-- Create the groups for users and developers.
CREATE GROUP users;
CREATE GROUP developers;

-- Create the user for the "jobrunner" script.
DROP USER IF EXISTS jobrunner;
CREATE USER jobrunner PASSWORD :new_password;
ALTER USER jobrunner SET search_path TO public, import;

-- Permission:  create new schemas in "data" database.
GRANT CREATE ON DATABASE transactions TO jobrunner;

-- Permission:  create temporary tables in "data" database.
GRANT TEMPORARY ON DATABASE transactions TO jobrunner, GROUP developers;

-- Permission:  select from tables and execute functions in "public" schema.
-- (Including any tables created by the 'jobrunner' user.)
-- Permission:  create new tables in the "public" schema.
GRANT USAGE ON SCHEMA public TO jobrunner, GROUP users, GROUP developers;
GRANT CREATE ON SCHEMA public TO jobrunner;
ALTER DEFAULT PRIVILEGES FOR USER jobrunner IN SCHEMA public
    GRANT SELECT ON TABLES TO GROUP users, GROUP developers;
ALTER DEFAULT PRIVILEGES FOR USER jobrunner IN SCHEMA public
    GRANT EXECUTE ON FUNCTIONS TO GROUP users, GROUP developers;

-- Permission:  create new user-defined functions
GRANT USAGE ON LANGUAGE plpythonu TO jobrunner, GROUP developers;

-- Permission:  select from tables and execute functions in "import" schema,
-- and use those tables in foreign key relationships.
CREATE SCHEMA import;
GRANT USAGE, CREATE ON SCHEMA import TO jobrunner;
GRANT USAGE ON SCHEMA import TO GROUP developers;
ALTER DEFAULT PRIVILEGES FOR USER jobrunner IN SCHEMA import
    GRANT SELECT, REFERENCES ON TABLES TO GROUP developers;

-- Finally, revoke all rights to the "public" schema that weren't explicitly
-- granted above.  (If we do this first, before ALTER DEFAULT PRIVILEGES, we
-- seem to hit an obscure ParAccel bug.)
REVOKE CREATE, USAGE ON SCHEMA public FROM PUBLIC;

-- Done!
COMMIT;

