#!/bin/sh
#############################################################
# Add a sftp user to machine.
#
# To run:
#         sudo ./add_sftp_user.sh <username> <password>
# Note: make sure the file is executable by you. ie:  chmod 500 add_sftp_user.sh
#############################################################
if [ $# -ne 2 ]; then
        echo "Usage: add_sftp_user.sh username password"
        exit 0
fi
useradd -d /data -s /bin/false -G $(grep sftp_users /etc/group | cut -d: -f3) -p `mkpasswd -m SHA-512 $2` $1
mkdir -p /data/$1/upload
chown root:sftp_users /data/$1
chown -R $1:sftp_users /data/$1/upload
echo "created sftp_user $1 password: $2"

