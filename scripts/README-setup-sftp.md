## Step-by-step guide

  

1.  Create an instance with sufficient space on amazon EC2 service
2.  Update software and restart services
    a.  sudo update
    b.  sudo apt install openssh-server
    c.  sudo systemctl stop ssh.service
    d.  sudo systemctl start ssh.service
    e.  sudo systemctl enable ssh.service
3.  Configure Service
    a.  Edit the /etc/ssh/sshd_config file, comment out the first Subsystem line,
        i.  #Subsystem sftp /usr/lib/openssh/sftp-server
    b.  Then add the line:
        i.  Subsystem sftp internal-sftp
    c.  Picture here: [https://screencast.com/t/KAuPBlc8kk](https://screencast.com/t/KAuPBlc8kk)
    d.  Add additional lines:
        i.  UsePAM yes
        ii.  Match Group sftp_users
            *  (Note: Assumes you have created a group called sftp_users)
        ii.  ChrootDirectory /data/%u
            *  (Note: Assumes your filesystem with the large diskspace is mounted on /data and the upload directory is /data/<company>/upload)
        iv.  ForceCommand internal-sftp
    e.  Picture here: [https://screencast.com/t/Ba6yb85PSx](https://screencast.com/t/Ba6yb85PSx)
4.  Restart services
    a.  sudo systemctl restart ssh.service
5.  Add groups sftp_user
    a.  sudo groupadd sftp_users
        *  (Note: Adding a group is done only once, no need to rerun this if the group exists. Look at the /etc/group file.)
6.  Use the shell script to auto add user to the sftp server, the script is located in [https://bitbucket.org/engaging/pipeline/src/dev/scripts/add_sftp_user.sh](https://bitbucket.org/engaging/pipeline/src/dev/scripts/add_sftp_user.sh)
    a.  ./add_sftp_user.sh <username> <password>

