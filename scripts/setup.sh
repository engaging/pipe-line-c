#!/usr/bin/env bash

sudo apt-get update
sudo apt-get -y dist-upgrade

sudo apt-get install python3 python3-pip gpgv2 python3-dev git

chmod go-xrw .ssh/id_rsa

git clone git@bitbucket.org:engaging/pipeline.git
cd pipeline/

sudo pip3 install -r requirements.txt

